class Employee

    attr_accessor :name, :surname, :fathername,
        :birthdate, :phone, :email, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 9 или 12" unless (args.length == 9 || args.length == 12)

        if args.length >= 9
            @name = args[0]
            @surname = args[1]
            @fathername = args[2]
            @birthdate = args[3]
            @phone = args[4]
            @email = args[5]
            @passport = args[6]
            @speciality = args[7]
            @adress = args[8]
            @experience = 0
        end

        if args.length >= 12
            @experience = args[9]

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[10]
            @prevWorkSalary = args[11]
        end
    end

    def to_s
        t = "ФИО: #{@surname} #{@name} #{@fathername} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end
end

class TestEmployee
    @@names = ["Ваня", "Федя", "Гриша", "Илья", "Карина", "Нина", "Катя", "Вероника", "Наташа", "Рома", "Владимир", "Иннакентий"]
    @@surnames = ["Петров", "Иванов", "Агасян", "Бужинский", "Елочкин", "Каличенко", "Фокин", "Монетков", "Бутылочкин", "Отверткин"]
    @@fathernames = ["Иванович", "Петрович", "Владимирович", "Геннадьевич", "Даниилович", "Васильевич", "Никитович", "Каренович", "Федерович", "Иннакентьевич"]
    @@birthdate = ["01.01.1978", "05.02.2004", "11.12.1921", "15.06.2001", "31.02.1965", "21.04.1971", "25.09.1972", "30.10.1985"]
    @@phones = ["+79955347123", "+79184325434", "+79182134954", "+79182353764", "+79953457234", "+79912417635"]
    @@emails = ["ivanovboy2004@mail.ru", "superman@gmail.com", "oldman@rambler.ru", "denis@mail.ru"]
    @@passports = ["0125 074561", "3425 158896", "0632 124978", "6386 657235", "1276 124467"]
    @@specialities = ["Уборщик", "Продавец", "Сантехник", "boss", "Грузчик", "Курьер"]
    @@adresses = ["Кранснодар, Ул. уличная, 124 кв 87", "Кранснодар, Ул. Петрова, 51 кв 44", "Кранснодар, Ул. Иванова, 63 кв 16"]

    def self.createFedor
        if rand() > 0.5
            Employee.new(@@names.sample, @@surnames.sample, @@fathernames.sample, @@birthdate.sample, @@phones.sample,
                @@emails.sample, @@passports.sample, @@specialities.sample, @@adresses.sample)

        else
            Employee.new(@@names.sample, @@surnames.sample, @@fathernames.sample, @@birthdate.sample, @@phones.sample,
                @@emails.sample, @@passports.sample, @@specialities.sample, @@adresses.sample, rand(10), @@specialities.sample, 5000 + rand(10000))
        end
    end
end

for i in 0 .. 10
    puts TestEmployee.createFedor
end
