class Employee

    attr_accessor :name, :surname, :fathername,
        :birthdate, :phone, :email, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 9 или 12" unless (args.length == 9 || args.length == 12)

        if args.length == 9
            @name = args[0]
            @surname = args[1]
            @fathername = args[2]
            @birthdate = args[3]
            @phone = args[4]
            @email = args[5]
            @passport = args[6]
            @speciality = args[7]
            @adress = args[8]
            @experience = 0
        end

        if args.length == 12
            @experience = args[9]

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[10]
            @prevWorkSalary = args[11]
        end
    end
end

youngEmp = Employee.new("Иванов", "Петр", "Федорович", "03.06.1946", "+79957517964",
                      "ivanovboy2004@mail.ru", "0315 224124", "Уборщик", "Петрова 35")
                      
oldEmp = Employee.new("Иванов", "Петр", "Федорович", "03.06.1946", "+79957517964",
                      "ivanovboy2004@mail.ru", "0315 224124", "Уборщик", "Петрова 35", 1,2,3)

puts "Old methods number: #{oldEmp.methods.length}"
puts "Young methods number: #{youngEmp.methods.length}"
