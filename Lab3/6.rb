class Employee

    attr_accessor :name,
        :birthdate, :phone, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 7 или 10" unless (args.length == 7 || args.length == 10)

        if args.length >= 7
            self.name = args[0]
            self.birthdate = args[1]
            self.phone = args[2]
            self.email = args[3]
            @passport = args[4]
            @speciality = args[5]
            @adress = args[6]
            @experience = 0
        end

        if args.length >= 10
            @experience = args[7].to_i

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[8]
            @prevWorkSalary = args[9].to_i
        end
    end

    def isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def isEmailCorrect?(email)
        email =~ /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ ? true : false
    end

    def prettyEmail(email)
        raise "Неверный формат почты: #{email}" unless isEmailCorrect? email.strip

        email.downcase
    end

    def email=(email)
        @email = prettyEmail(email)
    end

    def email
        @email
    end

    def isNameCorrect?(name)
        name =~ /([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)/ ? true : false
    end

    def prettyName(name)
        raise "Неверный формат имени: #{name}" unless isNameCorrect? name.strip
        name = name.downcase
        name = name.gsub(/\s\s+/, " ").gsub(/\b./, &:upcase).gsub(/(?<=[а-яА-Я])\s-\s(?=[а-яА-Я])/, "-")
    end

    def name=(name)
        @name = prettyName(name)
    end

    def name
        @name
    end

    def isBirthdateCorrect?(birthdate)
        birthdate =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def prettyBirthdate(birthdate)
        raise "Неверный формат даты: #{birthdate}" unless isBirthdateCorrect? birthdate.strip
        birthdate = birthdate.split('.').map {|i| i.to_i.to_s }
        birthdate[0] = "0" + birthdate[0] if birthdate[0].to_i < 10
        birthdate[1] = "0" + birthdate[1] if birthdate[1].to_i < 10
        birthdate[2] = "20" + birthdate[2] if birthdate[2].to_i < 100
        birthdate.join(".")
    end

    def birthdate=(birthdate)
        @birthdate = prettyBirthdate(birthdate)
    end

    def birthdate
        @birthdate
    end

    def to_s
        t = "ФИО: #{@surname} #{@name} #{@fathername} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end
end

youngEmp = Employee.new("Салтыков   -       щедрин иван-руслан Ахмед-заде", "0003.6.1946", "+7 945 74 14 7 64",
    "iVanoVbOy2004@mAil.ru", "0315 224124", "Уборщик", "Петрова 35")

sdgdssdhf = Employee.new("Салтыков-щедрин Руслан Ахмед", "03.06.46", "8-(995)-422-463-5",
    "fedORPEtr@soMeMail.ru", "0315 224124", "Уборщик", "Петрова 35")

puts youngEmp.birthdate
puts sdgdssdhf.birthdate
