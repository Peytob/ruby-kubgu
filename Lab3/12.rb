require 'openssl'

class Employee

    attr_accessor :name, :birthdate, :phone, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 7 или 10" unless (args.length == 7 || args.length == 10)

        if args.length >= 7
            self.name = args[0]
            self.birthdate = args[1]
            self.phone = args[2]
            self.email = args[3]
            self.passport = args[4]
            @speciality = args[5]
            @adress = args[6]
            @experience = 0
        end

        if args.length >= 10
            @experience = args[7].to_i

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[8]
            @prevWorkSalary = args[9].to_i
        end
    end

    def self.isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless Employee.isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = Employee.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def self.isEmailCorrect?(email)
        email =~ /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ ? true : false
    end

    def self.prettyEmail(email)
        raise "Неверный формат почты: #{email}" unless Employee.isEmailCorrect? email.strip

        email.downcase
    end

    def email=(email)
        @email = Employee.prettyEmail(email)
    end

    def email
        @email
    end

    def self.isNameCorrect?(name)
        name =~ /([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)/ ? true : false
    end

    def self.prettyName(name)
        raise "Неверный формат имени: #{name}" unless Employee.isNameCorrect? name.strip
        name = name.downcase
        name = name.gsub(/\s\s+/, " ").gsub(/\b./, &:upcase).gsub(/(?<=[а-яА-Я])\s-\s(?=[а-яА-Я])/, "-")
    end

    def name=(name)
        @name = Employee.prettyName(name)
    end

    def name
        @name
    end

    def self.isPassportCorrect?(passport)
        passport =~ /(\s*\d\s*){10}/ ? true : false
    end

    def self.prettyPassport(passport)
        raise "Неверный формат паспорта: #{passport}" unless Employee.isPassportCorrect? passport.strip
        passport.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("").insert(4, " ")
    end

    def passport=(passport)
        @passport = Employee.prettyPassport(passport)
    end

    def passport
        @passport
    end

    def self.isBirthdateCorrect?(birthdate)
        birthdate =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyBirthdate(birthdate)
        raise "Неверный формат даты: #{birthdate}" unless Employee.isBirthdateCorrect? birthdate.strip
        birthdate = birthdate.split('.').map {|i| i.to_i.to_s }
        birthdate[0] = "0" + birthdate[0] if birthdate[0].to_i < 10
        birthdate[1] = "0" + birthdate[1] if birthdate[1].to_i < 10
        birthdate[2] = "20" + birthdate[2] if birthdate[2].to_i < 100
        birthdate.join(".")
    end

    def birthdate=(birthdate)
        @birthdate = Employee.prettyBirthdate(birthdate)
    end

    def birthdate
        @birthdate
    end

    def to_s
        t = "ФИО: #{@surname} #{@name} #{@fathername} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end
end

class TestEmployee
    @@names = ["Ваня", "Федя", "Гриша", "Илья", "Карина", "Нина", "Катя", "Вероника", "Наташа", "Рома", "Владимир", "Иннакентий"]
    @@surnames = ["Петров", "Иванов", "Агасян", "Бужинский", "Елочкин", "Каличенко", "Фокин", "Монетков", "Бутылочкин", "Отверткин"]
    @@fathernames = ["Иванович", "Петрович", "Владимирович", "Геннадьевич", "Даниилович", "Васильевич", "Никитович", "Каренович", "Федерович", "Иннакентьевич"]
    @@birthdate = ["01.01.1978", "05.02.2004", "11.12.1921", "15.06.2001", "31.02.1965", "21.04.1971", "25.09.1972", "30.10.1985"]
    @@phones = ["+79955347123", "+79184325434", "+79182134954", "+79182353764", "+79953457234", "+79912417635"]
    @@emails = ["ivanovboy2004@mail.ru", "superman@gmail.com", "oldman@rambler.ru", "denis@mail.ru"]
    @@passports = ["0125 074561", "3425 158896", "0632 124978", "6386 657235", "1276 124467"]
    @@specialities = ["Уборщик", "Продавец", "Сантехник", "boss", "Грузчик", "Курьер"]
    @@adresses = ["Кранснодар, Ул. уличная, 124 кв 87", "Кранснодар, Ул. Петрова, 51 кв 44", "Кранснодар, Ул. Иванова, 63 кв 16"]

    def self.createRandom
        if rand() > 0.5
            return Employee.new(@@names.sample + " " + @@surnames.sample + " " + @@fathernames.sample, @@birthdate.sample, @@phones.sample,
                @@emails.sample, @@passports.sample, @@specialities.sample, @@adresses.sample)

        else
            return Employee.new(@@names.sample + " " + @@surnames.sample + " " + @@fathernames.sample, @@birthdate.sample, @@phones.sample,
                @@emails.sample, @@passports.sample, @@specialities.sample, @@adresses.sample, rand(10), @@specialities.sample, 5000 + rand(10000))
        end
    end
end

class Crypto
    private
        attr_writer :key, :iv
        attr_accessor :cipher
    public
        attr_reader :key, :iv

    def initialize(key = nil, iv = nil)
        self.cipher = OpenSSL::Cipher.new 'AES-256-CBC'
        if key == nil then generateKey else self.key = key end
        self.iv = iv if iv != nil
    end

    def generateKey
        self.iv = self.cipher.random_iv
        pwd = 'some hopefully not to easily guessable password'
        salt = OpenSSL::Random.random_bytes 16
        iter = 20000
        key_len = self.cipher.key_len
        digest = OpenSSL::Digest::SHA256.new
        self.key = OpenSSL::PKCS5.pbkdf2_hmac(pwd, salt, iter, key_len, digest)
    end

    def encrypt text
        self.cipher.encrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        encrypted = self.cipher.update text
        encrypted << self.cipher.final
        encrypted
    end

    def decrypt encrypted
        self.cipher.decrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        decrypted = self.cipher.update encrypted
        decrypted << self.cipher.final
        decrypted
    end

    def exportKeyToFile(filenameKey, filenameIv)
        File.open(filenameKey, 'wb') do
            |file|
            file.write(self.key)
        end

        File.open(filenameIv, 'wb') do
            |file|
            file.write(self.iv)
        end
    end

    def importKeyFromFile(filenameKey, filenameIv)
        self.key = File.open(filenameKey, 'rb').read
        self.iv = File.open(filenameIv, 'rb').read
    end
end

# crp1 = Crypto.new
# enc = crp1.encrypt "Some impotant text"
# crp1.exportKeyToFile("key", "iv")
# puts "decrypt crp1: #{crp1.decrypt(enc)}"

# crp2 = Crypto.new "", ""
# crp2.importKeyFromFile("key", "iv")
# puts crp1.key == crp2.key
# puts "decrypt crp2: #{crp2.decrypt(enc)}"

class TerminalViewListEmployee
    attr_accessor :list

    def initialize(putSomePioples = false)
        self.list = Array.new

        if putSomePioples
            for i in 0 ... 5 + (rand() * 15).to_i
                self.list << TestEmployee.createRandom
            end
        end
    end

    def fileDump(filename)
        # Разбиваются по строкам, так как нет символа-сепаратора, недоступного во всех
        # полях поэтому бьем по новым строкам

        crp = Crypto.new
        crp.exportKeyToFile("key", "iv")

        File.open(filename, 'w') do
            |file|

            self.list.each do
                |emp|

                file.write(emp.name + "\n")
                file.write(emp.birthdate + "\n")
                file.write(emp.phone + "\n")
                file.write(emp.email + "\n")
                file.write(crp.encrypt(emp.passport) + "\n")
                file.write(emp.speciality + "\n")
                file.write(emp.adress + "\n")
                file.write(emp.experience.to_s + "\n")

                file.write(emp.prevWorkName + "\n") if emp.experience.to_i != 0
                file.write(emp.prevWorkSalary.to_s + "\n") if emp.experience.to_i != 0
            end
        end
    end

    def fileLoad(filename)
        self.list.clear
        lines = File.open(filename, 'r').readlines.map { |line| line.chomp }
    
        crp = Crypto.new
        crp.importKeyFromFile("key", "iv")

        while !lines.empty?
            name = lines.delete_at 0
            birthdate = lines.delete_at 0
            phone = lines.delete_at 0
            email = lines.delete_at 0
            passport = crp.decrypt(lines.delete_at(0))
            speciality = lines.delete_at 0
            adress = lines.delete_at 0
            experience = (lines.delete_at 0).to_i

            if experience != 0
                prevWorkName = lines.delete_at 0
                prevWorkSalary = (lines.delete_at 0).to_i
                self.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress, experience, prevWorkName, prevWorkSalary)
            else
                self.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress)
            end
        end
    end

    def consoleInput
        print "Введите имя: "
        name = STDIN.gets.strip

        print "Введите дату рождения: "
        birthdate = STDIN.gets.strip
        
        print "Введите мобильный телефон: "
        phones = STDIN.gets.strip

        print "Введите почту: "
        emails = STDIN.gets.strip
        
        print "Введите паспортные данные: "
        passports = STDIN.gets.strip

        print "Введите специальность: "
        specialities = STDIN.gets.strip
        
        puts "Введите адрес проживания: "
        adresses = STDIN.gets.strip

        begin
            tvle.list << Employee.new(name, birthdate, phones, emails, passports, specialities, adresses)
        rescue RuntimeError => e
            puts e.message
        else
            puts "Что-то явно пошло не так, но ввод, вроде, верный"
        end
    end
end

tvle = TerminalViewListEmployee.new true
print "Введите команду (show - показать, add - добавить, dump - выгрузка в файл, load - загрузка из файла): "
command = STDIN.gets.strip

while !command.empty?
    if command == "show"
        if tvle.list.empty?
            puts "Empty"
        
        else
            NameFieldSize = tvle.list.map { |i| i.name.length }.max + 2
            Birthdate = 16
            Phones = 16
            Emails = tvle.list.map { |i| i.email.length }.max + 2
            Passports = 13
            Specialities = [tvle.list.map { |i| i.speciality.length }.max + 2, 15].max
            Adresses = tvle.list.map { |i| i.adress.length }.max + 2
            Exp = 6
            Salary = 10

            str = "#".to_s.center(5)
            str += "Имя".center(NameFieldSize)
            str += "Дата рождения".center(Birthdate)
            str += "Телефон".center(Phones)
            str += "Почта".center(Emails)
            str += "Паспорт".center(Passports)
            str += "Специальность".center(Specialities)
            str += "Адрес".center(Adresses)
            str += "Опыт".center(Exp)
            str += "Прош. спец".center(Specialities)
            str += "Зарплата".center(Salary)

            puts str

            tvle.list.each_with_index do
                |elem, index|
                
                str = index.to_s.center(5)
                str += elem.name.center(NameFieldSize)
                str += elem.birthdate.center(Birthdate)
                str += elem.phone.center(Phones)
                str += elem.email.center(Emails)
                str += elem.passport.center(Passports)
                str += elem.speciality.center(Specialities)
                str += elem.adress.center(Adresses)
                str += elem.experience.to_s.center(Exp)

                if elem.experience != 0
                    str += elem.prevWorkName.center(Specialities)
                    str += elem.prevWorkSalary.to_s.center(Salary)
                else
                    str += "X".center(Specialities)
                    str += "X".to_s.center(Salary)
                end
                puts str
            end
        end

    elsif command == "dump"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        tvle.fileDump(filename)

    elsif command == "load"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        tvle.fileLoad(filename)

    elsif command == "add"
        tvle.consoleInput
    end

    print "Введите команду (show - показать, add - добавить): "
    command = STDIN.gets.strip
end
