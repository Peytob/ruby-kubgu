class Employee

    attr_accessor :name, :surname, :fathername,
        :birthdate, :phone, :email, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 9 или 12" unless (args.length == 9 || args.length == 12)

        if args.length >= 9
            @name = args[0]
            @surname = args[1]
            @fathername = args[2]
            @birthdate = args[3]
            self.phone = args[4]
            @email = args[5]
            @passport = args[6]
            @speciality = args[7]
            @adress = args[8]
            @experience = 0
        end

        if args.length >= 12
            @experience = args[9]

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[10]
            @prevWorkSalary = args[11]
        end
    end

    def isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 3]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def to_s
        t = "ФИО: #{@surname} #{@name} #{@fathername} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end
end

youngEmp = Employee.new("Иванов", "Петр", "Федорович", "03.06.1946", "+7 945 74 14 7 64",
    "ivanovboy2004@mail.ru", "0315 224124", "Уборщик", "Петрова 35")

sdgdssdhf = Employee.new("Иванов", "Петр", "Федорович", "03.06.1946", "8-(995)-422-463-5",
        "ivanovboy2004@mail.ru", "0315 224124", "Уборщик", "Петрова 35")

puts youngEmp.phone
puts sdgdssdhf.phone
