class Employee

    attr_accessor :name,
        :birthdate, :phone, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 7 или 9" unless (args.length == 7 || args.length == 9)

        if args.length >= 7
            self.name = args[0]
            @birthdate = args[1]
            self.phone = args[2]
            self.email = args[3]
            @passport = args[4]
            @speciality = args[5]
            @adress = args[6]
            @experience = 0
        end

        if args.length >= 12
            @experience = args[7]

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[8]
            @prevWorkSalary = args[9]
        end
    end

    def isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 3]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def isEmailCorrect?(email)
        email =~ /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ ? true : false
    end

    def prettyEmail(email)
        raise "Неверный формат почты: #{email}" unless isEmailCorrect? email.strip

        email.downcase
    end

    def email=(email)
        @email = prettyEmail(email)
    end

    def email
        @email
    end

    def isNameCorrect?(name)
        name =~ /([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)/ ? true : false
    end

    def prettyName(name)
        raise "Неверный формат имени: #{name}" unless isNameCorrect? name.strip
        name = name.downcase
        name = name.gsub(/\s\s+/, " ").gsub(/\b./, &:upcase).gsub(/(?<=[а-яА-Я])\s-\s(?=[а-яА-Я])/, "-")
    end

    def name=(name)
        @name = prettyName(name)
    end

    def name
        @name
    end

    def to_s
        t = "ФИО: #{@surname} #{@name} #{@fathername} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end
end

youngEmp = Employee.new("Салтыков   -       щедрин иван-руслан Ахмед-заде", "03.06.1946", "+7 945 74 14 7 64",
    "iVanoVbOy2004@mAil.ru", "0315 224124", "Уборщик", "Петрова 35")

sdgdssdhf = Employee.new("Салтыков-щедрин Руслан Ахмед", "03.06.1946", "8-(995)-422-463-5",
    "fedORPEtr@soMeMail.ru", "0315 224124", "Уборщик", "Петрова 35")

puts youngEmp.name
puts sdgdssdhf.name
