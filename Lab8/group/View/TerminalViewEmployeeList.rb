require_relative "TerminalViewList.rb"

class TerminalViewEmployeeList < TerminalViewList
private

    attr_accessor :selected

public

    def initialize(controller)
        super(controller)
        self.selected = nil
    end

    ##
    # Выбор элемента
    def select
        print "Введите ID: "
        id = STDIN.gets
        self.selected = self.controller.employeeList.choose(id)
    end

    ##
    # Добавление рабочего
    def add
        print "Введите название департамента: "
        name = STDIN.gets

        empBuild["id"] = nil
        Введите имя:
        empBuild["name"] = STDIN.gets.strip
        print "Введите дату рождения:"
        empBuild["birthdate"] = STDIN.gets.strip
        print "Введите телефон:"
        empBuild["phone"] = STDIN.gets.strip
        print "Введите почту:"
        empBuild["email"] = STDIN.gets.strip
        print "Введите номер документа:"
        empBuild["passport"] = STDIN.gets.strip
        print "Введите специальность:"
        empBuild["speciality"] = STDIN.gets.strip
        print "Введите адрес:"
        empBuild["adress"] = STDIN.gets.strip
        print "Введите опыт работы:"
        empBuild["experience"] = STDIN.gets.strip.to_i

        if empBuild["experience"] != 0:
            Введите название предыдущей работы:
            empBuild["prevWorkName"] = STDIN.gets.strip
            Введите предыдущую зарплату:
            empBuild["prevWorkSalary"] = STDIN.gets.strip.to_i
        end

        self.controller.add(empBuild) # Тут выдается ID
    end

    ##
    # override
    def show(item)
        puts item
    end

    ##
    # override
    def showList(iterable)
        # idLen = iteraple.map {|emp| emp.id.to_s.length }
        # nameLen = iteraple.map {|emp| emp.name.length }
        # birthdateLen = iteraple.map {|emp| emp.birthdate.length }
        # phoneLen = iteraple.map {|emp| emp.phone.length }
        # emailLen = iteraple.map {|emp| emp.email.length }
        # passportLen = iteraple.map {|emp| emp.passport.length }
        # specialityLen = iteraple.map {|emp| emp.speciality.length }
        # adressLen = iteraple.map {|emp| emp.adress.length }
        # experienceLen = iteraple.map {|emp| emp.experience.to_s.length }

        # str = "%#{idLen}s%#{nameLen}s%#{birthdateLen}s%#{phoneLen}s%#{emailLen}s%#{passportLen}s%#{specialityLen}s%#{adressLen}s%#{experienceLen}s"
        # puts str % "id", "Имя", "Дата рождения", "Телефон", "Почта", "Паспорт", "Специал", "Адрес", "Опыт"
        iterable.each do
            |employee|
            # puts str % id, name, birthdate, phone, email, passport, speciality, adress, experience
            self.show(employee)
        end
    end

    ##
    # Показ выбранного элемента
    def showSelected
        puts if self.selected then self.show(self.selected) else "Ничего не выбрано" end
    end

    ##
    # Удаление выбранного элемента
    def deleteSelected
        self.controller.employeeList.delete(self.selected.id)
        self.controller.deleteInstance(self.selected)
    end
end
