require "mysql2"

require_relative "TerminalViewList.rb"
require_relative "../Model/Department.rb"

class TerminalViewDepartmentList < TerminalViewList
private

    attr_accessor :selected, :opened

public

    def initialize(controller)
        super(controller)
        self.selected = nil
        self.opened = true
    end

    def select
        print "Введите ID: "
        id = STDIN.gets.to_i

        if id == 0
            showError "Ожидалось ненулевое число"
            return
        end

        self.selected = self.controller.chooseInstance(id)

        if self.selected
            puts "Элемент выбран"
        else
            self.showError "Элемент не найден"
        end
    end

    def add
        print "Введите название департамента: "
        name = STDIN.gets.strip

        if name.empty?
            showError "Название не может быть пустым"

        else
            data = {"name" => name}

            begin
                self.controller.addInstance(data) # Тут выдается ID
            rescue Mysql2::Error::ConnectionError => e
                showError "SQL во время добавления. Обратитесь к умному человеку для исправления."
            rescue Exception => each
                showError "Произошла некоторая ошибка: #{each}"
            end
        end
    end

    # override
    def showItem(item)
        puts item
    end

    # override
    def show(departmentList)
        while self.opened
            puts "Меню"
            puts "-> 1 Отобразить список департаментов"
            puts "-> 2 Удалить выбранный департамент"
            puts "-> 3 Выйти из списка департаментов"
            puts "-> 4 Добавить департамент"
            puts "-> 5 Выбрать департамент"

            begin
                input = STDIN.gets.to_i
            rescue => exception
                showError "Ввод не является числом!"
                next
            end

            case input
            when 1
                self.showList(departmentList)
            when 2
                self.deleteSelected()
            when 3
                self.close()
            when 4
                self.add()
            when 5
                self.select()
            else
                showError "Неверный ввод пункта"
            end
        end
    end

    # override
    def showList(iterable)
        # nameLength = iterable.map { |department| department.name.length }
        # puts "%10s%#{nameLength}s" % "id", "Название"
        iterable.each do
            |department|
            # puts "%10s%#{nameLength}s" % department.id, department.name
            self.showItem(department)
        end
    end

    def showSelected
        if self.selected
            puts self.show(self.selected)
        else
            puts "Ничего не выбрано, нечего показывать"
        end
    end

    def deleteSelected
        if self.selected
            begin
                self.controller.deleteInstance(self.selected)
            rescue Mysql2::Error::ConnectionError => e
                showError "SQL во время добавления. Обратитесь к умному человеку для исправления."
            rescue Exception => each
                showError "Произошла некоторая ошибка"
            end
        else
            showError "Ничего не выбрано, нечего удалять!"
        end
    end

    # override
    def close
        self.opened = false
        self.controller.close
    end
end
