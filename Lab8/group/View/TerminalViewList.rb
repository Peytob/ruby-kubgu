
##
# Базовая вьюшка для отображения списков в терминале.
class TerminalViewList
protected

    # У листов одинаковый интерфейс, поэтому почему бы и нет
    attr_accessor :controller

public

    def initialize(controller)
        self.controller = controller
    end

    ##
    # Показывает окно терминала.
    def show
        raise NotEmplementedError, ":c"
    end

    ##
    # Показывает один элемент на экране.
    #
    # +item+ - элемент для вывода
    def showItem(item)
        raise NotEmplementedError, ":c"
    end

    ##
    # Вывод коллекции элементов на экран.
    #
    # +iterable+ - любая коллекция с возможностью итерации методами each, map ...
    def showList(iterable)
        raise NotEmplementedError, ":c"
    end

    ##
    # Закрытие окна терминала.
    def close
        raise NotEmplementedError, ":c"
    end

    ##
    # Вывод ошибки в терминал.
    def showError(text)
        puts "Ошибка: #{text}"
    end
end
