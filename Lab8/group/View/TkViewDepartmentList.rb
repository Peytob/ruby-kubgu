require "mysql2"
require 'tk'

require_relative "TerminalViewList.rb"
require_relative "../Model/Department.rb"

class TkViewDepartmentList < TerminalViewList
private

    attr_accessor :selected, :opened, :app, :newDepartmentTextEntry, :listBox

public

    def initialize(controller)
        super(controller)
        self.selected = nil
        self.opened = false

        # Вообще, это дело нужно в TkFrame обернуть
        # ЕСТЬ ЛИ ТУТ columnspan ВООБЩЕ????????

        self.app = TkRoot.new {
            title "Управление департаментами"
            setgrid
        }

        self.listBox = TkListbox.new(self.app) do
            grid ( {"column" => 0, "row" => 0} )
        end

        TkLabel.new(self.app) do
            text "А можно columnspan сюда? :с"
            grid ( {"column" => 1, "row" => 0} )
        end

        add = proc { self.add }
        TkButton.new(self.app) {
            text "Добавить департамент"
            grid ( {"column" => 0, "row" => 1} )
            command add
        }

        self.newDepartmentTextEntry = TkEntry.new(self.app) {
            grid ( {"column" => 1, "row" => 1} )
        }

        add = proc { self.deleteSelected }
        TkButton.new(self.app) {
            text "Удалить выбранный департамент"
            grid ( {"column" => 0, "row" => 2} )
            command add
        }
    end

    def add
        if self.newDepartmentTextEntry.value.empty?
            showError "Название не может быть пустым"

        else
            data = {"name" => self.newDepartmentTextEntry.value}

            begin
                self.controller.addInstance(data) # Тут выдается ID
                self.controller.showView()
            rescue Mysql2::Error::ConnectionError => e
                showError "SQL во время добавления. Обратитесь к умному человеку для исправления."
            rescue Exception => each
                showError "Произошла некоторая ошибка: #{each}"
            end
        end
    end

    # override
    def show(departmentList)
        self.showList(departmentList)

        self.app.mainloop unless self.opened
        self.opened = true
    end

    # override
    def showList(iterable)
        self.listBox.value = []
        iterable.each do
            |department|
            str = "#{department.id}, #{department.name}"
            self.listBox.insert 0, str
        end
    end

    def deleteSelected
        if !self.listBox.curselection.empty?
            begin
                selectedId = self.listBox.get(self.listBox.curselection).split(",")[0].to_i
                instance = self.controller.chooseInstance(selectedId)
                self.controller.deleteInstance(instance)
                self.controller.showView
            rescue Mysql2::Error::ConnectionError => e
                showError "SQL во время добавления. Обратитесь к умному человеку для исправления."
            rescue Exception => each
                showError "Произошла некоторая ошибка"
            end
        else
            showError "Ничего не выбрано, нечего удалять!"
        end
    end

    # override
    def close
        self.opened = false
        self.controller.close
    end

    def showError message
        Tk.messageBox ({
            'type'    => "ok",
            'icon'    => "info",
            'title'   => "Ошибка",
            'message' => message
        })
    end
end
