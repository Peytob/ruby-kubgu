require_relative "../Model/DepartmentList.rb"
require_relative "../Model/PostList.rb"

##
# Базовый класс для всех контроллеров списков
class ControllerList
protected
    attr_accessor :modelList
    attr_accessor :viewList
    attr_accessor :showFlag

    private_class_method :new

    def initialize
        self.showFlag = true
    end

    ##
    # Процесс общения с вьюшкой
    def showProcess
        raise NotEmplementedError, ":c"
    end

public

    ##
    # Включение показа вьюшки
    def showList
        self.showFlag = true
    end

    ##
    # Показ вьюшки, если включена
    def showView
        if self.showFlag
            self.showProcess
        end
    end

    ##
    # Выбор элемента
    # +id+ - ID элемента
    def chooseInstance(id)
        raise NotEmplementedError, ":c"
    end

    ##
    # Добавлеие элементов
    # +args+ - аргументы конструктора
    def addInstance(*args)
        raise NotEmplementedError, ":c"
    end

    ##
    # Удаление элемента
    # +id+ - ID элемента
    def deleteInstance(id)
        raise NotEmplementedError, ":c"
    end

    ##
    # Выключение показа вьюшки
    def closeList
        self.showFlag = false
    end
end