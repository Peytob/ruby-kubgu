class ControllerInstance
    def initialize

    end

    def showView()
        raise NotEmplementedError, ":c"
    end

    def save()
        raise NotEmplementedError, ":c"
    end

    def check()
        raise NotEmplementedError, ":c"
    end

    def cancel()
        raise NotEmplementedError, ":c"
    end
end