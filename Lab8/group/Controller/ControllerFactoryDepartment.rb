require_relative "ControllerFactory.rb"
require_relative "ControllerDepartmentList.rb"

class ControllerFactoryDepartment < ControllerFactory
    def create
        ControllerDepartmentList.new
    end
end
