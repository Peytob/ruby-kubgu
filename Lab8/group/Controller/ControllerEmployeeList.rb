require_relative "ControllerList.rb"

class ControllerEmployeeList
protected

    def showProcess
        self.modelList.readDB()
        self.viewList.showList(self.modelList)
    end

public

    public_class_method :new

    def initialize
        super()
        self.modelList = EmployeeList.new
        self.viewList = TerminalViewEmployeeList.new
    end

    def chooseInstance(id)
        self.modelList.choose(id)
    end

    def addInstance(*args)
        department = Department.new(nil, args["name"])
        self.modelList.add(department)
    end

    def deleteInstance(id)
        self.modelList.delete(id)
    end
end
