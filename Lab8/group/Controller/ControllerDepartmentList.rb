require_relative "ControllerList.rb"
require_relative "../View/TerminalViewDepartmentList.rb"
require_relative "../View/TkViewDepartmentList.rb"

class ControllerDepartmentList < ControllerList
protected

    def showProcess
        begin
            self.modelList.readDB()
            self.viewList.show(self.modelList)

        rescue Exception => e # Mysql2::Error::ConnectionError
            self.viewList.showError("Невозможно чтение данных из БД. Опять забыл про sudo systemctl start mysqld.service, дурачок!")
            puts e
        end
    end

public

    public_class_method :new

    def initialize
        super()
        self.modelList = DepartmentList.new
        self.viewList = ARGV.include?("graphics") ?
            TkViewDepartmentList.new(self) :
            TerminalViewDepartmentList.new(self)
    end

    def chooseInstance(id)
        self.modelList.choose(id)
    end

    def addInstance(args)
        department = Department.new(nil, args["name"])
        id = self.modelList.add(department)
        department.id = id
    end

    def deleteInstance(id)
        self.modelList.delete(id)
    end

    def close
        File.open("departments.json", 'w') do
            |file|
            file.write(modelList.to_json)
        end
    end
end
