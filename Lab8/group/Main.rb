# В данном файле содержится точка входа в программу.

require_relative "Controller/ControllerDepartmentList.rb"
require_relative "Controller/ControllerFactoryDepartment.rb"

##
# Класс, производящий начальную инициализацию приложения.
# Для вызова графической оболочки вызовите программу с ключом "graphics"
class Main

    ##
    # Создает начальный контроллер и вызывает отображение.
    #
    # +controllerCreator+ Начальный контроллер приложения.
    def self.main(controllerCreator)
        controllerDepartmentList = controllerCreator.create
        controllerDepartmentList.showView
    end
end

Main.main(ControllerFactoryDepartment.new)

# require 'tk'
# app = TkRoot.new {
#     title "Hello Tk!"; padx 50; pady 15
# }
# TkLabel.new(app) {
#     text "Hello, World!"
#     pack { padx 100; pady 100; side "left" }
# }
# Tk.mainloop
