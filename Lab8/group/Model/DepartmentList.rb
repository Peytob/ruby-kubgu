require_relative "DatabaseWork.rb"

class DepartmentList
protected

    attr_accessor :departments

public

    def initialize
        self.departments = Array.new
    end

    def each(&block)
        self.departments.each(&block)
    end

    def map(&block)
        self.departments.map(&block)
    end

    def add(department)
        raise ArgumentError.new("Department should be an object of Department class.") if department.class != Department
        self.departments << department
        DatabaseWork.getInstance.addDepartment(department)
    end

    def choose(id)
        self.departments.find { |department| department.id == id }
    end

    def edit(department)
        print "Введите поле для редактирования (1 - название): "
        loop do
            input = STDIN.gets.chomp.strip

            break if input.empty?

            case input
            when "1"
                department.name = STDIN.gets.chomp.strip
            else
                puts "Неверный ввод"
            end
        end

        DatabaseWork.getInstance.updateDepartament(department)
    end

    def delete(department)
        self.departments.delete(department)
        DatabaseWork.getInstance.deleteDepartment(department)
    end

    def readDB
        self.departments = []
        DatabaseWork.getInstance().selectAllDepartments(self)
        self.departments.each { |department| department.readDB }
    end

    def to_json
        departments = Array.new
        self.departments.each do
            |department|

            departments.push JSON.parse(department.to_json)
        end

        posts = Hash.new
        employees = Array.new
        self.departments.each do
            |department|

            postsList = JSON.parse(department.posts.to_json)

            postsList["posts"].each do
                |post|

                posts[post["id"]] = postsList.choose(post["id"])
                employees.push post.employee
            end
        end

        {
            "departments" => departments,
            "posts" => posts.map { |post| JSON.parse(post.to_json) },
            "employees" => employees.map { |employee| JSON.parse(employee.to_json) }
        }.to_json
    end
end
