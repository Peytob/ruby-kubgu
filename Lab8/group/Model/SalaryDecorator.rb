
##
# Высчет выплачиваемой зарплаты.
# Возможна реализация используя только ТРИ класса при помощи конструкции, описанной
# ниже:
#
# salary = JustSalary.new(QuartelySalary.new(OtherSalaryType.new ... ))
# В этом случае мы просто используем композицию из нескольких декораторов. Тогда имеет смысл
# оставить только классы JustSalary, FixedPremiumSalary, QuartelySalary.
# Данный механизм ИСПОЛЬЗУЕТСЯ в классе Post!
module SalaryDecorator
    class SalaryDecorator
        attr_accessor :target
        def initialize(target)
            self.target = target
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            raise NotImplementedError
        end
    end

    ##
    # Вариант декоратора с просто фиксированным окладом
    class JustSalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            target.get(mon)
        end
    end

    ##
    # Вариант декоратора с фиксированным окладом и фиксированными премиальными
    class FixedPremiumSalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            target.getWithPremium(mon)
        end
    end

    ##
    # Вариант декоратора с ежеквартальными выплатами
    class QuartelySalary
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            target.getWithQurtely(mon)
        end
    end

    ##
    # Вариант декоратора с фиксированным окладом и фиксированными премиальными с вкусняшками каждый квартал
    class FixedPremiumQuartelySalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            total = target.getWithPremium
            total + total * target.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
        end
    end

    ##
    # Вариант декоратора с возможным окладом
    class RandomBonusSalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            target.getWithPossibleBonus(mon)
        end
    end

    ##
    # Вариант декоратора с возможным окладом и обязательными премиальными
    class RandomBonusFixedSalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            target.getWithPossibleBonus(mon) + target.fixedPremium
        end
    end

    ##
    # Вариант декоратора с возможным окладом и ежеквартальными
    class RandomBonusQuartelySalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            total = target.getWithPossibleBonus(mon)
            total + total * target.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
        end
    end

    ##
    # Дорогой богатый вариант декоратора со всеми возможными надбавками
    class BurgeoisSalary < SalaryDecorator
        def initialize(target)
            super(target)
        end

        ##
        # Получение фиксированной зарплаты.
        #
        # +mon+ - номер месяца
        def get(mon)
            total = target.getWithPossibleBonus(mon) + target.fixed
            total + total * target.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
        end
    end
end
