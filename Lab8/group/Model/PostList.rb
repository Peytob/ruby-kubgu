require "json"
require_relative "ReadStrategy"

##
# Список должностей
class PostList
protected

    attr_accessor :posts, :strategy

public

    attr_accessor :department

    def initialize
        self.posts = Array.new
        self.strategy = ReadStrategy::ReadDBAll.new(self)
    end

    ##
    # Добавление новой должности
    #
    # +post+ - новая должность. Должна быть объектов класса Post!
    #
    # +ArgumentError+ - если +post+ не объект класса post
    # +MySqlError+ - если проблемы с конекшеном.
    def add(post)
        raise ArgumentError.new("Post should be an object of Post class.") if post.class != Post
        self.posts << post
        DatabaseWork.getInstance.addPost(post)
    end

    ##
    # Обновление должности
    #
    # +post+ - должность
    #
    # +MySqlError+ - если проблемы с конекшеном.
    def edit(post)
        DatabaseWork.getInstance.updatePost(post)
    end

    ##
    # Удаление должности
    #
    # +post+ - должность
    #
    # +MySqlError+ - если проблемы с конекшеном.
    def delete(post)
        DatabaseWork.getInstance.deletePost(post)
    end

    ##
    # Выбор должности по ID
    #
    # +ID+ - должность
    #
    # Return: должность, если найдена. nil, если не найдена
    def choose(id)
        self.posts.find { |post| post.id == id }
    end

    ##
    # Чтение из базы данных.
    def readDB
        self.strategy.read
    end

    def department=(dep)
        self.strategy = dep ? ReadStrategy::ReadDBDep.new(self) : ReadStrategy::ReadDBAll.new(self)
        @department = dep
    end

    def to_json
        posts = []
        self.posts.each do
            |post|

            posts.push JSON.parse(post.to_json)
        end

        {
            "posts" => posts
        }.to_json
    end
end
