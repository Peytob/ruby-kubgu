##
# Информация о зарплате рабочего.
class Salary
    attr_accessor :fixed, :quartelyPercent, :possibleBonusPercent

    def initialize(fixed, quartelyPercent, fixedPremium, possibleBonusPercent)
        self.fixed = fixed
        self.fixedPremium = fixedPremium
        self.quartelyPercent = quartelyPercent
        self.possibleBonusPercent = possibleBonusPercent
    end

    ##
    # Получение фиксированной зарплаты.
    #
    # +mon+ - номер месяца
    def get(mon)
        self.fixed
    end

    ##
    # Получение зарплаты с бонусом
    #
    # +mon+ - номер месяца
    def getWithPossibleBonus(mon)
        self.fixed + self.fixed * self.possibleBonusPercent * 0.25
    end

    ##
    # Получение зарплаты с премией
    #
    # +mon+ - номер месяца
    def getWithPremium(mon)
        self.fixed + self.fixedPremium
    end

    ##
    # Получение с ежеквартальной надбавкрй
    #
    # +mon+ - номер месяца
    def getWithQurtely(mon)
        self.fixed + self.fixed * self.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
    end
end
