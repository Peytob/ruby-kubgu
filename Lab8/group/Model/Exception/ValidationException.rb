class ValidationException < RuntimeError
    attr_reader :raisedClass, :fields
    def initialize(cls, fields)
        super("Validation fields exception: #{fields}")
        @raisedClass = cls
        @fields = fields
    end
end