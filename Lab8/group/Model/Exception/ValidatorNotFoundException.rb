class ValidatorNotFoundException < RuntimeError
    attr_reader :raisedClass
    def initialize(cls)
        super("Validator for #{cls} class is not defined")
        @raisedClass = cls
    end
end
