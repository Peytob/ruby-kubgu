require_relative "Exception/ValidatorNotFoundException.rb"
require_relative "Exception/ValidationException.rb"

##
# Шайтан-машина для валидации любых классов.
# Если в классе есть вложенный класс "Validator", содержащий
# статические методы с именами переменных, то класс вызывает
# соответствующие методы для проверки соответствующих полей.
# Пример в классе Department! Прям пример-пример в тестах (да, тут
# есть настоящие юнит-тесты)
# Кидает эксепшен, если что-то не прошло валидацию. В эксепшене при
# непрохождении валидации - поля, которые ее не прошли.

class Validator
public

    ##
    # Валидация некоторого объекта.
    #
    # +object+ - объект класса, внутри которого определен подкласс Validator (см описание
    # данного класса валидатора).
    def self.validate(object)
        raise ValidatorNotFoundException.new(object.class) unless defined?(object.class::Validator)

        incorrectList = object.instance_variables.map do
            |variable|

            variableLocal = variable[0] == "@" ? variable[1..-1] : variable
            result = object.class::Validator.send(variableLocal, object.send(variableLocal))
            result ? nil : variable
        end

        incorrectList.compact!
        raise ValidationException.new(object.class, incorrectList) unless incorrectList.empty?
    end

private
    private_class_method :new

end
