require "mysql2"
require_relative "Department.rb"
require_relative "Post.rb"

class DatabaseWork
private
    @@instance = nil

    attr_accessor :connection
    private_class_method :new

    # Формирует запрос select.
    def selectAll(table)
        self.connection.query("SELECT * FROM #{table};")
    end

    def initialize(username, password)
        self.connection = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => "stuff")
        self.connection.query('SELECT VERSION()').each { |row| } # просто проверка на вылет при чтении
    end

public

    def self.getInstance
        # @@instance |= self.new("user12", "34klq*") # Почему-то суют сюда true
        @@instance = self.new("user12", "34klq*") unless @@instance
        @@instance
    end

     # ==================================== DEPARTMENT

     def selectAllDepartments(target)
        self.selectAll("Departments").each {
            |department|

            target.add(Department.new(department["id"], department["name"]))
        }
    end

    def addDepartment(department)
        return if department.id

        sql = <<~EOF
            INSERT INTO `Departments` VALUES(NULL, '#{department.name}');
        EOF

        self.connection.query(sql)
        self.connection.query("SELECT LAST_INSERT_ID();").map { |id| id }[0]["LAST_INSERT_ID()"]
    end

    def updateDepartament(department)
        sql = <<~EOF
            UPDATE `Departments` SET
                name = '#{department.name}'

            WHERE id = #{department.id};
        EOF

        self.connection.query(sql)
    end

    def deleteDepartment(department)
        self.connection.query("DELETE FROM `Departments` WHERE id = #{department.id};")
    end

    # ====================================== POST

    def addPost(post)
        return if post.id

        sql = <<~EOF
            INSERT INTO `Post` VALUES(
                NULL,
                name '#{post.name}',
                fixedPremiumBool #{post.salary.fixedPremiumBool ? 1 : 0},
                possibleBonusBool #{post.salary.possibleBonusBool ? 1 : 0},
                quarterlyAwardBool #{post.salary.quarterlyAwardBool ? 1 : 0},
                fixedSalary #{post.salary.fixedSalary},
                quarterlyAwardSize #{post.salary.quarterlyAwardSize},
                fixedPremiumSize #{post.salary.fixedPremiumSize},
                possibleBonusPercent #{post.salary.possibleBonusPercent},
                departmentId #{post.departmentId},
                employeeId #{post.employee ? post.employee.id : 'NULL'}
            );
        EOF

        self.connection.query(sql)
        self.connection.query("SELECT LAST_INSERT_ID();").map { |id| id }[0]["LAST_INSERT_ID()"]
    end

    def selectAllPosts(target)
        self.selectAll("Post").each {
            |post|

            target.add(Post.new(post))
        }
    end

    def selectAllPostsFor(target, department)
        self.connection.query("SELECT * FROM `Post` WHERE departmentId = #{department.id};") {
            |department|

            target.add(Post.new(post))
        }
    end

    def updatePost(post)
        sql = <<~EOF
            UPDATE `Post` SET
                name = '#{post.name}',
                departmentId = #{post.departmentId},
                fixedSalary = #{post.salary.fixedSalary},
                quarterlyAwardSize = #{post.salary.quarterlyAwardSize},
                fixedPremiumSize = #{post.salary.fixedPremiumSize},
                possibleBonusPercent = #{post.salary.possibleBonusPercent},
                fixedPremiumBool = #{post.salary.fixedPremiumBool == true ? 1 : 0},
                possibleBonusBool = #{post.salary.possibleBonusBool == true ? 1 : 0},
                quarterlyAwardBool = #{post.salary.quarterlyAwardBool == true ? 1 : 0},
                employeeId = #{post.employee ? post.employee.id : 'NULL'}

            WHERE id = #{post.id};
        EOF

        self.connection.query(sql)
    end

    def deletePost(post)
        self.connection.query("DELETE FROM `Post` WHERE id = #{post.id};")
    end

    # ====================================== EMPLOYEE

    def selectAllEmployees(target)
        # {"id"=>1, "name"=>"Иванов Иван Иванович", "birtdate"=>#<Date: 2021-03-27 ((2459301j,0s,0n),+0s,2299161j)>,
        # "phone"=>"+7-918-2353764", "email"=>"some@mail.ru", "passport"=>"1234 123456", "speciality"=>"santex",
        # "adress"=>"Krasnodar", "experience"=>0, "prevWorkName"=>nil, "prevWorkSalary"=>nil}
        self.connection.query("SELECT * FROM Employee;").each do
            |row|
            name = row["name"]
            birtdate = row["birtdate"]
            birtdate = "#{birtdate.mday}.#{birtdate.mon}.#{birtdate.year}"
            phone = row["phone"]
            email = row["email"]
            passport = row["passport"]
            speciality = row["speciality"]
            adress = row["adress"]
            experience = row["experience"]

            if experience == 0
                target.add Employee.new(name, birtdate, phone, email, passport, speciality, adress)
            else
                prevWorkName = row["prevWorkName"]
                prevWorkSalary = row["prevWorkSalary"]
                target.add emp = Employee.new(name, birtdate, phone, email, passport, speciality, adress, prevWorkName, prevWorkSalary)
            end
        end
    end

    def addEmployee(emp)
        # insert into `Employee` values(NULL, CURDATE(), '+7-918-2353764',
        # 'some@mail.ru', '1234 123456', 'santex', 'Krasnodar', 0, NULL, NULL);

        preWork = "NULL"
        preSal = "NULL"
        if emp.experience != 0
            preWork = "'#{emp.prevWorkName}'"
            preSal = emp.prevWorkSalary
        end

        date = Date.strptime(emp.birthdate, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "INSERT INTO `Employee` values(NULL, '#{emp.name}', '#{date}', '#{emp.phone}', '#{emp.email}', '#{emp.passport}', '#{emp.speciality}', '#{emp.adress}', #{emp.experience}, #{preWork}, #{preSal});"
        self.connection.query(str)
    end

    def deleteEmployee(emp)
        self.connection.query("DELETE FROM `Employee` WHERE passport = '#{emp.passport}'")
    end

    def updateEmployee(emp, lastPas = nil)
        date = Date.strptime(emp.birthdate, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"

        preWork = "NULL"
        preSal = "NULL"
        if emp.experience != 0
            preWork = "'#{emp.prevWorkName}'"
            preSal = emp.prevWorkSalary
        end

        passport = if lastPas == nil then emp.passport else lastPas end

        str = "UPDATE `Employee` SET "
        str += "name = '#{emp.name}', "
        str += "birtdate = '#{date}', "
        str += "phone = '#{emp.phone}', "
        str += "email = '#{emp.email}', "
        str += "passport = '#{emp.passport}', "
        str += "speciality = '#{emp.speciality}', "
        str += "adress = '#{emp.adress}', "
        str += "experience = #{emp.experience}, "
        str += "prevWorkName = #{preWork}, "
        str += "prevWorkSalary = #{preSal} "
        str += "WHERE passport = '#{passport}';"

        self.connection.query(str)
    end
end
