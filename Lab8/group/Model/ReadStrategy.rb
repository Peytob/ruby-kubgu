require_relative "DatabaseWork.rb"
require_relative "Post.rb"

##
# Стратегия чтения данных из базы данных
module ReadStrategy
    ##
    # Базовый класс для стратегий чтения из БД
    class ReadingDBStarategy
        attr_accessor :target

        def initialize(target)
            self.target = target
        end

        ##
        # Чтение данных из базы (**неопределенный**)
        def read
            raise NotImplementedError.new("Method read is not emplemented!")
        end
    end

    ##
    # Чтение всех должностей
    class ReadDBAll < ReadingDBStarategy
        def initialize(target)
            super(target)
        end

        ##
        # Чтение всех данных из базы
        def read
            DatabaseWork.getInstance().selectAllPosts(target)
        end
    end

    ##
    # Чтение должностей только департамента
    class ReadDBDep < ReadingDBStarategy
        def initialize(target)
            super(target)
        end

        ##
        # Чтение данных определенного департамента из базы
        def read
            DatabaseWork.getInstance().selectAllPostsFor(target, self.target.department)
        end
    end
end
