require "json"
require_relative "Salary.rb"
require_relative "DatabaseWork.rb"
require_relative "SalaryDecorator.rb"

##
# Описание должности.
class Post
protected
    attr_writer :employee
public
    attr_accessor :salary, :name, :department, :id
    attr_reader :employee

    # row - из базы данных строка / просто хеш. Паттерн builder, можно сказать
    def initialize(row)
        self.id = row["id"]
        self.name = row["name"]
        self.department = row["departmentId"]

        salary = Salary.new(row["fixedSalary"], row["quarterlyAwardSize"], row["fixedPremiumSize"], row["possibleBonusPercent"])
        salary = SalaryDecorator::FixedPremiumSalary.new(salary) if row["fixedPremiumBool"] == 1
        salary = SalaryDecorator::RandomBonusSalary.new(salary) if row["possibleBonusBool"] == 1
        salary = SalaryDecorator::QuartelySalary.new(salary) if row["quarterlyAwardBool"] == 1
    end

    def employee=(emp)
        @employee = emp

        DatabaseWork.getInstance().updatePost(self) if self.id
    end

    def id=(id)
        raise "Post id is #{self.id}" if self.id
        @id = id
    end

    def to_json
        {
            "id" => self.id,
            "name" => self.name,
            "department" => self.department
        }.to_json
    end
end
