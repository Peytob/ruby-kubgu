require "json"
require_relative "DepartmentList.rb"

##
# Фасад для полной сереализации в 3 файла.
class MarshallFacade
    def marshallListYaml(departmentsList, filename)
        departments = JSON.parse(departmentsList.to_json)

        File.open(filename + "-departments.json", 'w') do
            |file|

            file.write(departments["departments"].to_json)
        end

        File.open(filename + "-posts.json", 'w') do
            |file|

            file.write(departments["posts"].to_json)
        end

        File.open(filename + "-employees.json", 'w') do
            |file|

            file.write(departments["employees"].to_json)
        end
    end
end

# dl = DepartmentList.new
# dl.readDB
# MarshallFacade.new.marshallListYaml(dl, "myfile")
