require "test/unit"
require_relative "../Model/Validator.rb"

# Два класса - один может проходить валидацию, другой нет

class ValidatorCanValidateThis
    class Validator
        def self.oneEnglishWord(data)
            return false unless data.instance_of? String
            data =~ /^[a-zA-Z]+$/ ? true : false
        end

        def self.numberFrom5to10(data)
            return false unless data.class.ancestors.include? Numeric
            data >= 5 && data <= 10
        end

        def self.anyFloat(data)
            data.instance_of? Float
        end
    end

    attr_accessor :oneEnglishWord, :numberFrom5to10, :anyFloat

    def initialize(oneEnglishWord, numberFrom5to10, anyFloat)
        self.oneEnglishWord = oneEnglishWord
        self.numberFrom5to10 = numberFrom5to10
        self.anyFloat = anyFloat
    end
end

class ValidatorOops
    attr_accessor :field1, :field2, :field3

    def initialize()
        self.field3 = 4
        self.field1 = 2
        self.field2 = 1
    end
end

class ValidatorNotFoundTest < Test::Unit::TestCase

    def test_simple
        assert_raise(ValidatorNotFoundException) { Validator.validate("Just string") }
        assert_raise(ValidatorNotFoundException) { Validator.validate(ValidatorOops.new) }
        Validator.validate( ValidatorCanValidateThis.new("eng", 7.0, 7.0) )
    end

end

class ValidationTest < Test::Unit::TestCase

    def test_simple
        badObject = ValidatorCanValidateThis.new("hello world", 12.0, 7.0)

        assert_raise(ValidationException) { Validator.validate(badObject) }

        begin
            Validator.validate(badObject)
        rescue ValidationException => e
            assert_equal([:@numberFrom5to10, :@oneEnglishWord], e.fields.sort)
        end
    end

end
