require 'openssl'
require 'mysql2'
require 'date'
require 'yaml'
require 'json'
require 'nokogiri'

class Employee

    attr_accessor :name, :birthdate, :phone, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 7 или 10" unless (args.length == 7 || args.length == 10)

        if args.length >= 7
            self.name = args[0]
            self.birthdate = args[1]
            self.phone = args[2]
            self.email = args[3]
            self.passport = args[4]
            @speciality = args[5]
            @adress = args[6]
            @experience = 0
        end

        if args.length >= 10
            return if args[8] == nil || args[8] == nil

            @experience = args[7].to_i

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[8]
            @prevWorkSalary = args[9].to_i
        end
    end

    def self.isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless Employee.isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = Employee.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def self.isEmailCorrect?(email)
        email =~ /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ ? true : false
    end

    def self.prettyEmail(email)
        raise "Неверный формат почты: #{email}" unless Employee.isEmailCorrect? email.strip

        email.downcase
    end

    def email=(email)
        @email = Employee.prettyEmail(email)
    end

    def email
        @email
    end

    def self.isNameCorrect?(name)
        name =~ /([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)/ ? true : false
    end

    def self.prettyName(name)
        raise "Неверный формат имени: #{name}" unless Employee.isNameCorrect? name.strip
        name = name.downcase
        name = name.gsub(/\s\s+/, " ").gsub(/\b./, &:upcase).gsub(/(?<=[а-яА-Я])\s-\s(?=[а-яА-Я])/, "-")
    end

    def name=(name)
        @name = Employee.prettyName(name)
    end

    def name
        @name
    end

    def self.isPassportCorrect?(passport)
        passport =~ /(\s*\d\s*){10}/ ? true : false
    end

    def self.prettyPassport(passport)
        raise "Неверный формат паспорта: #{passport}" unless Employee.isPassportCorrect? passport.strip
        passport.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("").insert(4, " ")
    end

    def passport=(passport)
        @passport = Employee.prettyPassport(passport)
    end

    def passport
        @passport
    end

    def self.isBirthdateCorrect?(birthdate)
        birthdate =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyBirthdate(birthdate)
        raise "Неверный формат даты: #{birthdate}" unless Employee.isBirthdateCorrect? birthdate.strip
        birthdate = birthdate.split('.').map {|i| i.to_i.to_s }
        birthdate[0] = "0" + birthdate[0] if birthdate[0].to_i < 10
        birthdate[1] = "0" + birthdate[1] if birthdate[1].to_i < 10
        birthdate[2] = "20" + birthdate[2] if birthdate[2].to_i < 100
        birthdate.join(".")
    end

    def birthdate=(birthdate)
        @birthdate = Employee.prettyBirthdate(birthdate)
    end

    def birthdate
        @birthdate
    end

    def to_s
        t = "ФИО: #{self.name} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end

    def to_json
        emp = {
            "name" => self.name,
            "birthdate" => self.birthdate,
            "phone" => self.phone,
            "email" => self.email,
            "passport" => self.passport,
            "speciality" => self.speciality,
            "adress" => self.adress
        }

        if self.experience == 0
            emp["experience"] = 0
            emp["prevWorkName"] = nil
            emp["prevWorkSalary"] = nil

        else
            emp["experience"] = self.experience
            emp["prevWorkName"] = self.prevWorkName
            emp["prevWorkSalary"] = self.prevWorkSalary
        end

        emp.to_json
    end

    def to_xmlNode doc
        root = Nokogiri::XML::Node.new "employee", doc
        (root.add_child Nokogiri::XML::Node.new "name", doc).content = self.name
        (root.add_child Nokogiri::XML::Node.new "birthdate", doc).content = self.birthdate
        (root.add_child Nokogiri::XML::Node.new "phone", doc).content = self.phone
        (root.add_child Nokogiri::XML::Node.new "email", doc).content = self.email
        (root.add_child Nokogiri::XML::Node.new "passport", doc).content = self.passport
        (root.add_child Nokogiri::XML::Node.new "speciality", doc).content = self.speciality
        (root.add_child Nokogiri::XML::Node.new "adress", doc).content = self.adress
        (root.add_child Nokogiri::XML::Node.new "experience", doc).content = self.experience
    
        if self.experience != 0
            (root.add_child Nokogiri::XML::Node.new "prevWorkName", doc).content = self.prevWorkName
            (root.add_child Nokogiri::XML::Node.new "prevWorkSalary", doc).content = self.prevWorkSalary
        end

        root
    end

    def eql?(other)
        return false if other.class != self.class
        return false if other.experience != self.experience

        r = true
        r &= self.name == other.name
        r &= self.birthdate == other.birthdate
        r &= self.phone == other.phone
        r &= self.email == other.email
        r &= self.passport == other.passport
        r &= self.speciality == other.speciality
        r &= self.adress == other.adress

        if self.experience != 0 # У второго тоже не ноль - см выше
            r &= self.prevWorkName == other.prevWorkName
            r &= self.prevWorkSalary == other.prevWorkSalary
        end

        r
    end
end

class Crypto
    private
        attr_writer :key, :iv
        attr_accessor :cipher
    public
        attr_reader :key, :iv

    def initialize(key = nil, iv = nil)
        self.cipher = OpenSSL::Cipher.new 'AES-256-CBC'
        if key == nil then generateKey else self.key = key end
        self.iv = iv if iv != nil
    end

    def generateKey
        self.iv = self.cipher.random_iv
        pwd = 'some hopefully not to easily guessable password'
        salt = OpenSSL::Random.random_bytes 16
        iter = 20000
        key_len = self.cipher.key_len
        digest = OpenSSL::Digest::SHA256.new
        self.key = OpenSSL::PKCS5.pbkdf2_hmac(pwd, salt, iter, key_len, digest)
    end

    def encrypt text
        self.cipher.encrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        encrypted = self.cipher.update text
        encrypted << self.cipher.final
        encrypted
    end

    def decrypt encrypted
        self.cipher.decrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        decrypted = self.cipher.update encrypted
        decrypted << self.cipher.final
        decrypted
    end

    def exportKeyToFile(filenameKey, filenameIv)
        File.open(filenameKey, 'wb') do
            |file|
            file.write(self.key)
        end

        File.open(filenameIv, 'wb') do
            |file|
            file.write(self.iv)
        end
    end

    def importKeyFromFile(filenameKey, filenameIv)
        self.key = File.open(filenameKey, 'rb').read
        self.iv = File.open(filenameIv, 'rb').read
    end
end

class ListEmployee
    attr_accessor :list

    def initialize
        self.list = Array.new
    end

    def consoleInput
        print "Введите имя: "
        name = STDIN.gets.strip

        print "Введите дату рождения: "
        birthdate = STDIN.gets.strip
        
        print "Введите мобильный телефон: "
        phones = STDIN.gets.strip

        print "Введите почту: "
        emails = STDIN.gets.strip
        
        print "Введите паспортные данные: "
        passports = STDIN.gets.strip

        print "Введите специальность: "
        specialities = STDIN.gets.strip
        
        puts "Введите адрес проживания: "
        adresses = STDIN.gets.strip

        begin
            self.list << Employee.new(name, birthdate, phones, emails, passports, specialities, adresses)
        rescue RuntimeError => e
            puts e.message
        rescue Exception => e
            puts "Что-то явно пошло не так, но ввод, вроде, верный: #{e.message}"
        end

        return self.list.last
    end

    def show
        if self.list.empty?
            puts "Empty"
        
        else
            nameFieldSize = list.map { |i| i.name.length }.max + 2
            birthdate = 16
            phones = 16
            emails = list.map { |i| i.email.length }.max + 2
            passports = 13
            specialities = [list.map { |i| i.speciality.length }.max + 2, 15].max
            adresses = list.map { |i| i.adress.length }.max + 2
            exp = 6
            salary = 10

            str = "#".to_s.center(5)
            str += "Имя".center(nameFieldSize)
            str += "Дата рождения".center(birthdate)
            str += "Телефон".center(phones)
            str += "Почта".center(emails)
            str += "Паспорт".center(passports)
            str += "Специальность".center(specialities)
            str += "Адрес".center(adresses)
            str += "Опыт".center(exp)
            str += "Прош. спец".center(specialities)
            str += "Зарплата".center(salary)

            puts str

            list.each_with_index do
                |elem, index|
                
                str = index.to_s.center(5)
                str += elem.name.center(nameFieldSize)
                str += elem.birthdate.center(birthdate)
                str += elem.phone.center(phones)
                str += elem.email.center(emails)
                str += elem.passport.center(passports)
                str += elem.speciality.center(specialities)
                str += elem.adress.center(adresses)
                str += elem.experience.to_s.center(exp)

                if elem.experience != 0
                    str += elem.prevWorkName.center(specialities)
                    str += elem.prevWorkSalary.to_s.center(salary)
                else
                    str += "X".center(specialities)
                    str += "X".to_s.center(salary)
                end
                puts str
            end
        end
    end

    def findByName(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.name === value }
        findLst
    end

    def findByEmail(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.email === value }
        findLst
    end

    def findByPassport(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.passport === value }
        findLst
    end

    def findByPhoneNumber(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.phone === value }
        findLst
    end

    def sortByName()
        self.list = self.list.sort_by { |emp| emp.name }
    end

    def sortByPhone()
        self.list = self.list.sort_by { |emp| emp.phone }
    end

    def writeListYAML(filename)
        File.open(filename, "w") do
            |file|
            YAML.dump(self.list, file)
        end
    end

    def readListYAML(filename)
        self.list = YAML.load_file(filename)
    end

    def to_json
        jsonList = self.list.map { |i| i.to_json }.join(", ")
        "[" + jsonList + "]"
        # Почему оно переводит массив в что-то типа {["{}", "{}", ...]} ???
    end

    def writeListJSON(filename)
        File.open(filename, "w") do
            |file|
            file.write(self.to_json) # Переводит emp в строку
        end
    end

    def readListJSON(filename)
        json = ""
        File.open(filename, 'r') do
            |file|

            json = file.read
        end

        JSON.parse(json).each do
            |empJson|
            
            puts empJson
            emp = Employee.new(empJson["name"], empJson["birthdate"], empJson["phone"], empJson["email"], empJson["passport"], empJson["speciality"], empJson["adress"], empJson["experience"], empJson["prevWorkName"], empJson["prevWorkSalary"])
            self.list.push emp
        end
    end

    def writeListXML(filename)
        doc = Nokogiri::XML "<employees></employees>"
        root = doc.at_css("employees")

        self.list.each { |emp|  root.add_child (emp.to_xmlNode doc) }

        File.open(filename, 'w') do
            |file|

            file.write(doc)
        end
    end

    def readListXML(filename)
        doc = File.open(filename, 'r').read
        doc = Nokogiri::XML doc

        return if !doc.errors.empty?

        # puts doc

        doc.css("employee").map do
            |node|

            puts node.css("name").text
            puts node.css("agdsg").text.dump

            name = node.css("name").text
            birthdate = node.css("birthdate").text
            phone = node.css("phone").text
            email = node.css("email").text
            passport = node.css("passport").text
            speciality = node.css("speciality").text
            adress = node.css("adress").text
            experience = node.css("experience").text
            prevWorkName = node.css("prevWorkName").text
            prevWorkSalary = node.css("prevWorkSalary").text

            self.list.push Employee.new(name, birthdate, phone, email, passport, speciality, adress, experience, prevWorkName, prevWorkSalary)
        end
    end
end

class DatabaseConnection
private
    attr_accessor :connection

public
    def initialize(username, password)
        self.connection = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => "stuff")
        self.connection.query('SELECT VERSION()').each do
            |row|
    
            puts "Версия базы данных: #{row["VERSION()"]}"
        end
    end

    def readListDB
        # {"id"=>1, "name"=>"Иванов Иван Иванович", "birtdate"=>#<Date: 2021-03-27 ((2459301j,0s,0n),+0s,2299161j)>,
        # "phone"=>"+7-918-2353764", "email"=>"some@mail.ru", "passport"=>"1234 123456", "speciality"=>"santex",
        # "adress"=>"Krasnodar", "experience"=>0, "prevWorkName"=>nil, "prevWorkSalary"=>nil}

        acc = ListEmployee.new

        self.connection.query("SELECT * FROM Employee;").each do
            |row|
            name = row["name"]
            birtdate = row["birtdate"]
            birtdate = "#{birtdate.mday}.#{birtdate.mon}.#{birtdate.year}"
            phone = row["phone"]
            email = row["email"]
            passport = row["passport"]
            speciality = row["speciality"]
            adress = row["adress"]
            experience = row["experience"]

            if experience == 0
                acc.list.push Employee.new(name, birtdate, phone, email, passport, speciality, adress)
            else
                prevWorkName = row["prevWorkName"]
                prevWorkSalary = row["prevWorkSalary"]
                acc.list.push emp = Employee.new(name, birtdate, phone, email, passport, speciality, adress, prevWorkName, prevWorkSalary)
            end
        end

        acc
    end

    def add(emp)
        # insert into `Employee` values(NULL, CURDATE(), '+7-918-2353764',
        # 'some@mail.ru', '1234 123456', 'santex', 'Krasnodar', 0, NULL, NULL);

        preWork = "NULL"
        preSal = "NULL"
        if emp.experience != 0
            preWork = "'#{emp.prevWorkName}'"
            preSal = emp.prevWorkSalary
        end

        date = Date.strptime(emp.birthdate, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "INSERT INTO `Employee` values(NULL, '#{emp.name}', '#{date}', '#{emp.phone}', '#{emp.email}', '#{emp.passport}', '#{emp.speciality}', '#{emp.adress}', #{emp.experience}, #{preWork}, #{preSal});"
        self.connection.query(str)
    end

    def delete emp
        self.connection.query("DELETE FROM `Employee` WHERE passport = '#{emp.passport}'")
    end
end

class TerminalViewListEmployee
private
    attr_accessor :listEmp, :database

public

    def initialize dumpFile
        begin
            self.database = DatabaseConnection.new("user12", "34klq*")
            self.listEmp = self.database.readListDB()
        rescue Exception => exp
            puts "Ошибка при инициализации: #{exp}"
            print "Загрузка из файла дампа: #{dumpFile} (Д/н)?"
            t = STDIN.gets.strip.downcase
            abort "На нет и суда нет" if t == "" || t == "н" || t == "n"
            self.listEmp = ListEmployee.new
            self.loadFrom(dumpFile)
            return
        end

        if File.exists? dumpFile
            fileEmp = ListEmployee.new
            fileEmp.list = self.listEmp.list.map { |emp| emp.clone() }
            fileEmp.sortByName
            self.loadFrom(dumpFile)
            self.listEmp.sortByName
            equal = self.listEmp.list.zip(fileEmp.list).inject(true) { |acc, first| acc & first[0].eql?(first[1]) }
            equal &= self.listEmp.list.size == fileEmp.list.size
            if !equal
                print "Данные в дампе и БД не являются одинаковыми! Откуда брать (Ф / бд)? "
                source = STDIN.gets.strip.downcase
                if source == "бд"
                    self.listEmp = fileEmp
                else
                    self.database.clear
                    self.listEmp.each { |emp| self.database.db}
                end
            end
        end
    end 

    def fileDump(filename)
        # Разбиваются по строкам, так как нет символа-сепаратора, недоступного во всех
        # полях поэтому бьем по новым строкам

        crp = Crypto.new
        crp.exportKeyToFile("key", "iv")

        File.open(filename, 'w') do
            |file|

            self.listEmp.list.each do
                |emp|

                file.write(emp.name + "\n")
                file.write(emp.birthdate + "\n")
                file.write(emp.phone + "\n")
                file.write(emp.email + "\n")
                file.write(crp.encrypt(emp.passport) + "\n")
                file.write(emp.speciality + "\n")
                file.write(emp.adress + "\n")
                file.write(emp.experience.to_s + "\n")

                file.write(emp.prevWorkName + "\n") if emp.experience.to_i != 0
                file.write(emp.prevWorkSalary.to_s + "\n") if emp.experience.to_i != 0
            end
        end
    end

    def fileLoad(filename)
        self.listEmp.list.clear
        lines = File.open(filename, 'r').readlines.map { |line| line.chomp }
    
        crp = Crypto.new
        crp.importKeyFromFile("key", "iv")

        while !lines.empty?
            name = lines.delete_at 0
            birthdate = lines.delete_at 0
            phone = lines.delete_at 0
            email = lines.delete_at 0
            passport = crp.decrypt(lines.delete_at(0))
            speciality = lines.delete_at 0
            adress = lines.delete_at 0
            experience = (lines.delete_at 0).to_i

            if experience != 0
                prevWorkName = lines.delete_at 0
                prevWorkSalary = (lines.delete_at 0).to_i
                self.listEmp.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress, experience, prevWorkName, prevWorkSalary)
            else
                self.listEmp.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress)
            end
        end
    end

    def dumpAs(filename)

        type = filename.split('.')[1]

        case type
        when "yaml"
            self.listEmp.writeListYAML(filename)

        when "json"
            self.listEmp.writeListJSON(filename)

        when "xml"
            self.listEmp.writeListXML(filename)

        else
            puts "Неверный формат"
        end
    end

    def loadFrom(filename)
        self.listEmp.list.clear

        type = filename.split('.')[1]

        case type
        when "yaml"
            self.listEmp.readListYAML(filename)

        when "json"
            self.listEmp.readListJSON(filename)

        when "xml"
            self.listEmp.readListXML(filename)

        else
            puts "Неверный формат"
        end
    end

    def consoleInput
        emp = self.listEmp.consoleInput
        self.database.add(emp)
    end

    def show
        self.listEmp.show
    end

    def find
        puts <<~"END"
        Поиск по:
        1. ФИО
        2. Email
        3. Красный советский документ
        4. Номер телефона
        END

        print "Выбор: "
        
        case STDIN.gets.chomp.to_i
            when 1
                print "Имя: "
                begin
                    name = STDIN.gets.chomp
                    name = Employee.prettyName(name)
                    res = listEmp.findByName(name)
                    res.show
                rescue RuntimeError => e
                    puts e.message
                end

            when 2
                print "Адрес: "
                begin
                    email = STDIN.gets.chomp
                    email = Employee.prettyEmail(email)
                    res = listEmp.findByEmail(email)
                    res.show
                rescue RuntimeError => e
                    puts e.message
                end

        when 3
            print "Серия и номер: "
            begin
                passport = STDIN.gets.chomp
                passport = Employee.prettyPassport(passport)
                res = listEmp.findByPassport(passport)
                res.show
            rescue RuntimeError => e
                puts e.message
            end

        when 4
            print "Номер: "
            begin
                number = STDIN.gets.chomp
                number = Employee.prettyPhoneNumber(number)
                res = listEmp.findByPhoneNumber(number)
                res.show
            rescue RuntimeError => e
                puts e.message
            end
        end
    end

    def delete(id)
        emp = self.listEmp.list.delete_at id
        self.database.delete emp
    end

    def changeNode(emp, lastPas = nil)
        date = Date.strptime(emp.birthdate, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"

        preWork = "NULL"
        preSal = "NULL"
        if emp.experience != 0
            preWork = "'#{emp.prevWorkName}'"
            preSal = emp.prevWorkSalary
        end

        passport = if lastPas == nil then emp.passport else lastPas end

        str = "UPDATE `Employee` SET "
        str += "name = '#{emp.name}', "
        str += "birtdate = '#{date}', "
        str += "phone = '#{emp.phone}', "
        str += "email = '#{emp.email}', "
        str += "passport = '#{emp.passport}', "
        str += "speciality = '#{emp.speciality}', "
        str += "adress = '#{emp.adress}', "
        str += "experience = #{emp.experience}, "
        str += "prevWorkName = #{preWork}, "
        str += "prevWorkSalary = #{preSal} "
        str += "WHERE passport = '#{passport}';"

        puts str

        self.database.query(str)
    end

    def edit(id)
        puts "Редактирование пользователя с ID = #{id}"
        message = "1 - имя, 2 - дата рождения, 3 - телефон, 4 - почта, 5 - пасспорт, 6 - специальность, 7 - адрес"
        puts message
        print "Выбор: "
        ch = STDIN.gets.chomp
        pas = self.listEmp.list[id].passport

        while !ch.empty?
            el = ch.to_i

            begin
                case el
                    when 1
                        self.listEmp.list[id].name = STDIN.gets.strip()

                    when 2
                        self.listEmp.list[id].birthdate = STDIN.gets.strip()

                    when 3
                        self.listEmp.list[id].phone = STDIN.gets.strip()

                    when 4
                        self.listEmp.list[id].email = STDIN.gets.strip()

                    when 5
                        self.listEmp.list[id].passport = STDIN.gets.strip()

                    when 6
                        self.listEmp.list[id].specialities = STDIN.gets.strip()

                    when 7
                        self.listEmp.list[id].adress = STDIN.gets.strip()
                end

            rescue Exception => e
                puts "Данные введены неверно, проверьте ввод. #{e}"
            end

            print "Выбор: "
            ch = STDIN.gets.chomp
        end

        self.changeNode(self.listEmp.list[id], pas)
    end

    def sort
        message = "1 - имя, 2 - телефон"
        puts message
        print "Выбор: "
        ch = STDIN.gets.chomp

        while !ch.empty?
            el = ch.to_i

            case el
                when 1
                    self.listEmp.sortByName
                when 2
                    self.listEmp.sortByPhone
            end

            print "Выбор: "
            ch = STDIN.gets.chomp
        end
    end
end

tvle = TerminalViewListEmployee.new "dump.yaml"
message = "Введите команду (show - показать, add - добавить, dump - выгрузка в файл, load - загрузка из файла, find - поиск, edit - редактирование по id, delete - удаление по id, sort - сортировка exit - выход): "
print message
command = STDIN.gets.strip

while !command.empty?
    if command == "show"
        tvle.show

    elsif command == "dump"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        tvle.fileDump(filename)

    elsif command == "load"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        tvle.fileLoad(filename)

    elsif command == "add"
        tvle.consoleInput

    elsif command == "find"
        tvle.find

    elsif command == "edit"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            tvle.edit(id)
        rescue Exception => exp
            puts exp.message
        end

    elsif command == "delete"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            tvle.delete(id)
        rescue Exception => exp
            puts exp.message
        end

    elsif command == "sort"
        tvle.sort

    elsif command == "exit"
        exit 0
    end

    print message
    command = STDIN.gets.strip
end

tvle.dumpAs "dump.yaml"
