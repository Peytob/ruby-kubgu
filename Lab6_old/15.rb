# sudo systemctl start mysqld.service
# CREATE USER user12@localhost IDENTIFIED BY '34klq*';

require 'mysql2'

class InputCorrector
    def self.isPhoneNumberCorrect?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrect? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def self.isDateCorrect?(date)
        date =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyDate(date)
        raise "Неверный формат даты: #{date}" unless isDateCorrect? date.strip
        date = date.split('.').map {|i| i.to_i.to_s }
        date[0] = "0" + date[0] if date[0].to_i < 10
        date[1] = "0" + date[1] if date[1].to_i < 10
        date[2] = "20" + date[2] if date[2].to_i < 100
        date.join(".")
    end
end

class Philial
    attr_accessor :title, :adress
    attr_reader :databaseId

    def initialize(title, adress, phone, id = -1)
        self.title = title
        self.adress = adress
        self.phone = phone
        @databaseId = id
    end

    def to_s
        "Филиал: #{self.title} по адресу #{self.adress} (телефон: #{self.phone})"
    end

    def phone=(phone)
        @phone = InputCorrector.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end
end

class Contract
    attr_accessor :type, :tariffRate,
        :insuranceAmount, :date

    def initialize(type, date, philial, tariffRate, insuranceAmount)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{self.philial.title} (тарифная ставка: #{tariffRate}; страховая сумма: #{self.insuranceAmount})"
    end

    def date=(date)
        @date = InputCorrector.prettyDate(date)
    end

    def date
        @date
    end

    def philial=(philial)
        raise TypeError.new("Philial field should be Philial class") if philial.class != Philial

        @philial = philial
    end

    def philial
        @philial
    end
end

begin
    $mySqlClient = Mysql2::Client.new(:host => "localhost", :username => "user12", :password => "34klq*", :database => "insurance")
    $mySqlClient.query('SELECT VERSION()').each do
        |row|

        puts "Версия базы данных: #{row["VERSION()"]}"
    end
    
rescue Mysql2::Error => e
    puts e.errno
    puts e.error
end

def createTables()
    $mySqlClient.query("CREATE TABLE IF NOT EXISTS Philials(
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(128) NOT NULL,
        phone VARCHAR(14) NOT NULL,
        adress VARCHAR(128) NOT NULL)")

    $mySqlClient.query("CREATE TABLE IF NOT EXISTS Contracts(
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        type VARCHAR(128) NOT NULL,
        date DATE NOT NULL,
        tariffRate INT NOT NULL,
        insuranceAmount INT NOT NULL,
        philialId INT,
        FOREIGN KEY (philialId)  REFERENCES Philials (id))")
end

def testSelectRawRes()
    $mySqlClient.query("SELECT * FROM Philials").each do
        |row|
        puts row
    end

    $mySqlClient.query("SELECT * FROM Contracts").each do
        |row|
        puts row
    end
end

def testSelect()
    philials = Array.new

    $mySqlClient.query("SELECT * FROM Philials").each do
        |row|

        philials.push Philial.new(row["title"], row["adress"], row["phone"], row["id"])
    end

    contracts = Array.new
    $mySqlClient.query("SELECT * FROM Contracts").each do
        |row|

        philial = philials.find { |p| p.databaseId == row["philialId"] }
        date = "#{row["date"].mday}.#{row["date"].mon}.#{row["date"].year}"
        contracts.push Contract.new(row["type"], date, philial, row["tariffRate"], row["insuranceAmount"])
    end

    [philials, contracts]
end

createTables()
puts "testSelectRawRes: "
testSelectRawRes()
puts ""
puts "testSelect: "
philials, contracts = testSelect()
for i in philials
    puts i
end
for i in contracts
    puts i
end

$mySqlClient.close

# insert into `Employee` values(NULL, CURDATE(), '+7-918-2353764', 'some@mail.ru', '1234 123456', 'santex', 'Krasnodar', 0, NULL, NULL);
