# sudo systemctl start mysqld.service
# CREATE USER user12@localhost IDENTIFIED BY '34klq*';

require 'mysql2'
require './Employee.rb'

begin
    $mySqlClient = Mysql2::Client.new(:host => "localhost", :username => "user12", :password => "34klq*", :database => "stuff")
    # $mySqlClient.
    $mySqlClient.query('SELECT VERSION()').each do
        |row|

        puts "Версия базы данных: #{row["VERSION()"]}"
    end
    
rescue Mysql2::Error => e
    puts e.errno
    puts e.error
end

def createEmpTable()
    $mySqlClient.query("CREATE TABLE IF NOT EXISTS Employee(
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(128) NOT NULL,
        birtdate DATE NOT NULL,
        phone VARCHAR(14) NOT NULL,
        email VARCHAR(64) NOT NULL,
        passport VARCHAR(64) NOT NULL UNIQUE,
        speciality VARCHAR(64) NOT NULL,
        adress VARCHAR(128) NOT NULL,
        experience INT NOT NULL,
        prevWorkName VARCHAR(64),
        prevWorkSalary INT)")
end

def testSelectRawRes()
    $mySqlClient.query("SELECT * FROM Employee").each do
        |row|

        puts row
    end
end

def testSelect()
    acc = Array.new

    $mySqlClient.query("SELECT * FROM Employee").each do
        |row|

        date = "#{row['birtdate'].day}.#{row['birtdate'].month}.#{row['birtdate'].year}"

        if row["experience"] == 0
            acc << Employee.new(row["name"], date, row["phone"], row["email"], row["passport"], row["speciality"],
                row["adress"])
        
        else
            acc << Employee.new(row["name"], date, row["phone"], row["email"], row["passport"], row["speciality"],
                row["adress"], row["experience"], row["prevWorkName"], row["prevWorkSalary"])
        end
    end

    acc
end

createEmpTable()
puts "testSelectRawRes: "
testSelectRawRes()
puts ""
puts "testSelectRawRes: "
for i in testSelect()
    puts i
end

$mySqlClient.close

# insert into `Employee` values(NULL, CURDATE(), '+7-918-2353764', 'some@mail.ru', '1234 123456', 'santex', 'Krasnodar', 0, NULL, NULL);
