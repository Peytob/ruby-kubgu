# sudo systemctl start mysqld.service
# CREATE USER user12@localhost IDENTIFIED BY '34klq*';

require 'mysql2'

begin
    $mySqlClient = Mysql2::Client.new(:host => "localhost", :username => "user12", :password => "34klq*", :database => "stuff")
    # $mySqlClient.
    $mySqlClient.query('SELECT VERSION()').each do
        |row|

        puts "Версия базы данных: #{row["VERSION()"]}"
    end

rescue Mysql2::Error => e
    puts e.errno
    puts e.error
end

def createEmpTable()
    $mySqlClient.query("CREATE TABLE IF NOT EXISTS Employee(
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(128),
        birtdate DATE NOT NULL,
        phone VARCHAR(14) NOT NULL,
        email VARCHAR(64) NOT NULL,
        passport VARCHAR(64) NOT NULL,
        speciality VARCHAR(64) NOT NULL,
        adress VARCHAR(128) NOT NULL,
        experience INT NOT NULL,
        prevWorkName VARCHAR(64),
        prevWorkSalary INT)")
end

createEmpTable()
$mySqlClient.close
