require 'mysql2'
require 'date'
require 'yaml'

class InputCorrector
    def self.isPhoneNumberCorrect?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrect? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def self.isDateCorrect?(date)
        date =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyDate(date)
        raise "Неверный формат даты: #{date}" unless isDateCorrect? date.strip
        date = date.split('.').map {|i| i.to_i.to_s }
        date[0] = "0" + date[0] if date[0].to_i < 10
        date[1] = "0" + date[1] if date[1].to_i < 10
        date[2] = "20" + date[2] if date[2].to_i < 100
        date.join(".")
    end
end

class Philial
    attr_accessor :title, :adress
    attr_reader :databaseId

    def initialize(title, adress, phone, id = -1)
        self.title = title
        self.adress = adress
        self.phone = phone
        @databaseId = id
    end

    def to_s
        "Филиал: #{self.title} по адресу #{self.adress} (телефон: #{self.phone})"
    end

    def phone=(phone)
        @phone = InputCorrector.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end
end

class Contract
    attr_accessor :type, :tariffRate,
        :insuranceAmount, :philial, :date, :dbid

    def initialize(type, date, philial, tariffRate, insuranceAmount, dbid = -1)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
        self.dbid = dbid
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{self.philial.title} (тарифная ставка: #{tariffRate}; страховая сумма: #{self.insuranceAmount})"
    end

    def date=(date)
        @date = InputCorrector.prettyDate(date)
    end

    def date
        @date
    end

    def philial=(philial)
        raise TypeError.new("Philial field should be Philial class") if philial.class != Philial

        @philial = philial
    end

    def philial
        @philial
    end
end

class DatabaseConnection
private
    attr_accessor :connection

public
    def initialize(username, password)
        self.connection = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => "insurance")
        self.connection.query('SELECT VERSION()').each do
            |row|
    
            puts "Версия базы данных: #{row["VERSION()"]}"
        end
    end

    def readListDB
        philials = Array.new

        self.connection.query("SELECT * FROM Philials;").each do
            |row|
            title = row["title"]
            phone = row["phone"]
            adress = row["adress"]
            id = row["id"]

            philials.push << Philial.new(title, adress, phone, id)
        end

        contracts = Array.new
        self.connection.query("SELECT * FROM Contracts;").each do
            |row|
            type = row["type"]
            date = row["date"]
            date = "#{date.mday}.#{date.mon}.#{date.year}"
            philial = philials.find { |ph| ph.databaseId == row["philialId"] }
            tariffRate = row["tariffRate"]
            insuranceAmount = row["insuranceAmount"]

            contracts.push << Contract.new(type, date, philial, tariffRate, insuranceAmount, row["id"])
        end

        ImpotantDatabase.new philials, contracts
    end

    def add(contract)
        # insert into `Philials` values(NULL, 'Организация страховок', '+7-918-2353764', 'Яблоновский 666');
        # insert into `Contracts` values(NULL, 'Страхование жизни', '2020-11-27', 5000, 1000000, 2);

        date = Date.strptime(contract.date, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "INSERT INTO `Contracts` values(NULL, '#{contract.type}', '#{date}', #{contract.tariffRate}, #{contract.insuranceAmount}, #{contract.philial.databaseId});"
        self.connection.query(str)
        self.connection.query("LAST_INSERT_ID()") { |id| contract.dbid = id }
    end

    def delete(contract)
        str = "DELETE FROM `Contracts` WHERE type = '#{contract.type}' AND tariffRate = #{contract.tariffRate} AND insuranceAmount = #{contract.insuranceAmount} AND philialId = #{contract.philial.databaseId};"
        self.connection.query(str)
    end

    def edit(contract)
        date = Date.strptime(contract.date, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "UPDATE `Contracts` SET "
        str += "type = '#{contract.type}', "
        str += "date = '#{date}', "
        str += "tariffRate = '#{contract.tariffRate}', "
        str += "insuranceAmount = '#{contract.insuranceAmount}', "
        str += "philialId = '#{contract.philialId}', "
        str += "WHERE id = '#{contract.dbid}';"
    end
end

class ImpotantDatabase
    attr_accessor :philials, :contracts

    def initialize(philials = [], contracts = [])
        self.philials = philials
        self.contracts = contracts
    end

    def show()
        puts "=== ФИЛИАЛЫ ==="
        self.philials.filter_map.with_index  { |e, i| puts "#{i + 1} : #{e}" }
        puts "=== КОНТРАКТЫ ==="
        self.contracts.filter_map.with_index { |e, i| puts "#{i + 1} : #{e}" }
    end

    def input()
        print "Введите тип: "
        type = STDIN.gets.chomp.strip

        print "Введите дату: "
        date = STDIN.gets.chomp.strip

        print "Введите номер филиала (см show): "
        philial = STDIN.gets.chomp.strip.to_i

        print "Введите тарифную ставку: "
        tariffRate = STDIN.gets.chomp.strip.to_i

        print "Введите размер выплаты: "
        insuranceAmount = STDIN.gets.chomp.strip.to_i

        self.contracts.push Contract.new(type.chomp, date.chomp, self.philials[philial], tariffRate, insuranceAmount)
    end

    def findByDate(date)
        date = InputCorrector.prettyDate(date)

        self.contracts.each_with_index { |e, i| puts "#{i + 1} : #{e}" if e.date == date }
    end

    def findByType(type)
        self.contracts.each_with_index { |e, i| puts "#{i + 1} : #{e}" if e.type == type }
    end

    def edit(id)
        message = "Изменить: 1 - тип, 2 - дату, 3 - тарифную ставку, 4 - размер выплаты "
        print message
        number = STDIN.gets.chomp.strip
        while !number.empty?
            number = number.to_i

            case number
            when 1
                self.contracts[id].type = STDIN.gets.chomp.strip

            when 2
                self.contracts[id].date = STDIN.gets.chomp.strip

            when 3
                self.contracts[id].tariffRate = STDIN.gets.chomp.strip.to_i

            when 4
                self.contracts[id].insuranceAmount = STDIN.gets.chomp.strip.to_i

            else
                puts "Введено что-то не то"
            end

            print message
            number = STDIN.gets.chomp.strip
        end
    end

    def delete(id)
        id = id - 1
        self.contracts.delete_at(id)
    end

    def sortByDate
        self.contracts = self.contracts.sort_by { |i| i.date }
    end

    def sortByPhilialName
        self.contracts = self.contracts.sort_by { |i| i.philial.title }
    end

    def sortByTariff
        self.contracts = self.contracts.sort_by { |i| i.tariffRate }
    end

    def toYaml(filename)
        File.open(filename + "-philials.yaml", "w") do
            |file|
            YAML.dump(self.philials, file)
        end

        File.open(filename + "-contracts.yaml", "w") do
            |file|
            YAML.dump(self.contracts, file)
        end
    end

    def fromYaml(filename)
        self.philials = YAML.load_file(filename + "-philials.yaml")
        self.contracts = YAML.load_file(filename + "-contracts.yaml")
    end
end

class TerminalView
    attr_accessor :database, :connection

    def initialize(fill = false)
        self.connection = DatabaseConnection.new("user12", "34klq*")
        self.database = self.connection.readListDB
        self.database.sortByTariff

        databaseYaml = ImpotantDatabase.new
        databaseYaml.sortByTariff

        puts "Данные в БД и дампе не совпали. Взяты данные из БД" if self.database.eql?(databaseYaml)
    end

    def show()
        self.database.show()
    end

    def toFile(filename)
        index = 0
        File.open(filename + "-philials", 'w') do
            |file|

            self.database.philials.each do
                |philial|

                file.write(index.to_s + "\n")
                file.write(philial.title + "\n")
                file.write(philial.adress + "\n")
                file.write(philial.phone + "\n")

                index += 1
            end
        end

        File.open(filename + "-contracts", 'w') do
            |file|

            self.database.contracts.each do
                |contract|

                file.write(contract.type + "\n")
                file.write(contract.date + "\n")
                file.write(self.database.philials.index(contract.philial).to_s + "\n")
                file.write(contract.tariffRate.to_s + "\n")
                file.write(contract.insuranceAmount.to_s + "\n")

                index += 1
            end
        end
    end

    def fromFile(filename)
        self.database.philials.clear
        philialsLines = File.open(filename + "-philials", 'r').readlines

        while !philialsLines.empty?
            index = philialsLines.delete_at(0).to_i
            title = philialsLines.delete_at(0)
            adress = philialsLines.delete_at(0)
            phone = philialsLines.delete_at(0)

            self.database.philials[index] = Philial.new(title.chomp, adress.chomp, phone.chomp)
        end

        self.database.contracts.clear
        contractsLines = File.open(filename + "-contracts", 'r').readlines

        while !contractsLines.empty?
            type = contractsLines.delete_at(0)
            date = contractsLines.delete_at(0)
            philial = self.database.philials[contractsLines.delete_at(0).to_i]
            tariffRate = contractsLines.delete_at(0).to_i
            insuranceAmount = contractsLines.delete_at(0).to_i

            self.database.contracts.push Contract.new(type.chomp, date.chomp, philial, tariffRate, insuranceAmount)
        end
    end

    def input
        self.database.input
        self.connection.add(self.database.contracts[-1])
    end

    def find
        print "Найти по: 1 - дата, 2 - тип"
        number = STDIN.gets.chomp.strip.to_i
        print "Запрос: "
        what = STDIN.gets.chomp.strip
        case number
        when 1
            self.database.findByDate what

        when 2
            self.database.findByType what

        else
            puts "Введено что-то не то"
        end
    end

    def edit(id)
        self.database.edit(id - 1)
        self.connection.add(self.database.contracts[id - 1])
    end

    def delete(id)
        self.connection.delete(self.database.contracts[id - 1])
        self.database.delete(id - 1)
    end
    
    def sort
        print "Сортировать по: 1 - дата, 2 - название филиала, 3 - тарифную ставку "
        number = STDIN.gets.chomp.strip.to_i
        case number
        when 1
            self.database.sortByDate

        when 2
            self.database.sortByPhilialName

        when 3
            self.database.sortByTariff

        else
            puts "Введено что-то не то"
        end
    end
end

terminal = TerminalView.new
terminal.show
message = "Введите команду (show - показать, add - добавить, dump - выгрузка в файл, load - загрузка из файла, find - поиск, edit - редактирование по id, delete - удаление по id, sort - сортировка exit - выход): "
print message
command = STDIN.gets.strip

while !command.empty?
    if command == "show"
        terminal.show

    elsif command == "dump"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        terminal.fileDump(filename)

    elsif command == "load"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        terminal.fileLoad(filename)

    elsif command == "add"
        terminal.consoleInput

    elsif command == "find"
        terminal.find

    elsif command == "edit"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            terminal.edit(id)
        rescue
            puts "Айдишка точно является числом?.."
        end

    elsif command == "delete"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            terminal.delete(id)
        rescue
            puts "Айдишка точно является числом?.."
        end

    elsif command == "sort"
        terminal.sort

    elsif command == "exit"
        terminal.toYaml("dump")
        exit 0
    end

    print message
    command = STDIN.gets.strip
end

terminal.toYaml("dump")
