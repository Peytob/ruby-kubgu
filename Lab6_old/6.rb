require 'openssl'
require 'mysql2'
require 'date'

class Employee

    attr_accessor :name, :birthdate, :phone, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 7 или 10" unless (args.length == 7 || args.length == 10)

        if args.length >= 7
            self.name = args[0]
            self.birthdate = args[1]
            self.phone = args[2]
            self.email = args[3]
            self.passport = args[4]
            @speciality = args[5]
            @adress = args[6]
            @experience = 0
        end

        if args.length >= 10
            @experience = args[7].to_i

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[8]
            @prevWorkSalary = args[9].to_i
        end
    end

    def self.isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless Employee.isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = Employee.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def self.isEmailCorrect?(email)
        email =~ /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ ? true : false
    end

    def self.prettyEmail(email)
        raise "Неверный формат почты: #{email}" unless Employee.isEmailCorrect? email.strip

        email.downcase
    end

    def email=(email)
        @email = Employee.prettyEmail(email)
    end

    def email
        @email
    end

    def self.isNameCorrect?(name)
        name =~ /([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)/ ? true : false
    end

    def self.prettyName(name)
        raise "Неверный формат имени: #{name}" unless Employee.isNameCorrect? name.strip
        name = name.downcase
        name = name.gsub(/\s\s+/, " ").gsub(/\b./, &:upcase).gsub(/(?<=[а-яА-Я])\s-\s(?=[а-яА-Я])/, "-")
    end

    def name=(name)
        @name = Employee.prettyName(name)
    end

    def name
        @name
    end

    def self.isPassportCorrect?(passport)
        passport =~ /(\s*\d\s*){10}/ ? true : false
    end

    def self.prettyPassport(passport)
        raise "Неверный формат паспорта: #{passport}" unless Employee.isPassportCorrect? passport.strip
        passport.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("").insert(4, " ")
    end

    def passport=(passport)
        @passport = Employee.prettyPassport(passport)
    end

    def passport
        @passport
    end

    def self.isBirthdateCorrect?(birthdate)
        birthdate =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyBirthdate(birthdate)
        raise "Неверный формат даты: #{birthdate}" unless Employee.isBirthdateCorrect? birthdate.strip
        birthdate = birthdate.split('.').map {|i| i.to_i.to_s }
        birthdate[0] = "0" + birthdate[0] if birthdate[0].to_i < 10
        birthdate[1] = "0" + birthdate[1] if birthdate[1].to_i < 10
        birthdate[2] = "20" + birthdate[2] if birthdate[2].to_i < 100
        birthdate.join(".")
    end

    def birthdate=(birthdate)
        @birthdate = Employee.prettyBirthdate(birthdate)
    end

    def birthdate
        @birthdate
    end

    def to_s
        t = "ФИО: #{@surname} #{@name} #{@fathername} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end
end

class Crypto
    private
        attr_writer :key, :iv
        attr_accessor :cipher
    public
        attr_reader :key, :iv

    def initialize(key = nil, iv = nil)
        self.cipher = OpenSSL::Cipher.new 'AES-256-CBC'
        if key == nil then generateKey else self.key = key end
        self.iv = iv if iv != nil
    end

    def generateKey
        self.iv = self.cipher.random_iv
        pwd = 'some hopefully not to easily guessable password'
        salt = OpenSSL::Random.random_bytes 16
        iter = 20000
        key_len = self.cipher.key_len
        digest = OpenSSL::Digest::SHA256.new
        self.key = OpenSSL::PKCS5.pbkdf2_hmac(pwd, salt, iter, key_len, digest)
    end

    def encrypt text
        self.cipher.encrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        encrypted = self.cipher.update text
        encrypted << self.cipher.final
        encrypted
    end

    def decrypt encrypted
        self.cipher.decrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        decrypted = self.cipher.update encrypted
        decrypted << self.cipher.final
        decrypted
    end

    def exportKeyToFile(filenameKey, filenameIv)
        File.open(filenameKey, 'wb') do
            |file|
            file.write(self.key)
        end

        File.open(filenameIv, 'wb') do
            |file|
            file.write(self.iv)
        end
    end

    def importKeyFromFile(filenameKey, filenameIv)
        self.key = File.open(filenameKey, 'rb').read
        self.iv = File.open(filenameIv, 'rb').read
    end
end

class ListEmployee
    attr_accessor :list

    def initialize
        self.list = Array.new
    end

    def consoleInput
        print "Введите имя: "
        name = STDIN.gets.strip

        print "Введите дату рождения: "
        birthdate = STDIN.gets.strip
        
        print "Введите мобильный телефон: "
        phones = STDIN.gets.strip

        print "Введите почту: "
        emails = STDIN.gets.strip
        
        print "Введите паспортные данные: "
        passports = STDIN.gets.strip

        print "Введите специальность: "
        specialities = STDIN.gets.strip
        
        puts "Введите адрес проживания: "
        adresses = STDIN.gets.strip

        begin
            tvle.list << Employee.new(name, birthdate, phones, emails, passports, specialities, adresses)
        rescue RuntimeError => e
            puts e.message
        else
            puts "Что-то явно пошло не так, но ввод, вроде, верный"
        end
    end

    def show
        if self.list.empty?
            puts "Empty"
        
        else
            nameFieldSize = list.map { |i| i.name.length }.max + 2
            birthdate = 16
            phones = 16
            emails = list.map { |i| i.email.length }.max + 2
            passports = 13
            specialities = [list.map { |i| i.speciality.length }.max + 2, 15].max
            adresses = list.map { |i| i.adress.length }.max + 2
            exp = 6
            salary = 10

            str = "#".to_s.center(5)
            str += "Имя".center(nameFieldSize)
            str += "Дата рождения".center(birthdate)
            str += "Телефон".center(phones)
            str += "Почта".center(emails)
            str += "Паспорт".center(passports)
            str += "Специальность".center(specialities)
            str += "Адрес".center(adresses)
            str += "Опыт".center(exp)
            str += "Прош. спец".center(specialities)
            str += "Зарплата".center(salary)

            puts str

            list.each_with_index do
                |elem, index|
                
                str = index.to_s.center(5)
                str += elem.name.center(nameFieldSize)
                str += elem.birthdate.center(birthdate)
                str += elem.phone.center(phones)
                str += elem.email.center(emails)
                str += elem.passport.center(passports)
                str += elem.speciality.center(specialities)
                str += elem.adress.center(adresses)
                str += elem.experience.to_s.center(exp)

                if elem.experience != 0
                    str += elem.prevWorkName.center(specialities)
                    str += elem.prevWorkSalary.to_s.center(salary)
                else
                    str += "X".center(specialities)
                    str += "X".to_s.center(salary)
                end
                puts str
            end
        end
    end

    def findByName(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.name === value }
        findLst
    end

    def findByEmail(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.email === value }
        findLst
    end

    def findByPassport(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.passport === value }
        findLst
    end

    def findByPhoneNumber(value)
        lst = self.list.clone()
        findLst = ListEmployee.new
        findLst.list = lst.filter { |emp| emp.phone === value }
        findLst
    end

    def sortByName()
        self.list = self.list.sort_by { |emp| emp.name }
    end

    def sortByPhone()
        self.list = self.list.sort_by { |emp| emp.phone }
    end
end

class TerminalViewListEmployee
private
    attr_accessor :listEmp, :database

public

    def initialize
        self.listEmp = ListEmployee.new

        self.database = Mysql2::Client.new(:host => "localhost", :username => "user12", :password => "34klq*", :database => "stuff")
        self.database.query('SELECT VERSION()').each do
            |row|
    
            puts "Версия базы данных: #{row["VERSION()"]}"
        end

        self.readListDB()
    end

    def readListDB
        # {"id"=>1, "name"=>"Иванов Иван Иванович", "birtdate"=>#<Date: 2021-03-27 ((2459301j,0s,0n),+0s,2299161j)>,
        # "phone"=>"+7-918-2353764", "email"=>"some@mail.ru", "passport"=>"1234 123456", "speciality"=>"santex",
        # "adress"=>"Krasnodar", "experience"=>0, "prevWorkName"=>nil, "prevWorkSalary"=>nil}

        self.database.query("SELECT * FROM Employee;").each do
            |row|
            name = row["name"]
            birtdate = row["birtdate"]
            birtdate = "#{birtdate.year}.#{birtdate.mon}.#{birtdate.mday}"
            phone = row["phone"]
            email = row["email"]
            passport = row["passport"]
            speciality = row["speciality"]
            adress = row["adress"]
            experience = row["experience"]

            if experience == 0
                self.listEmp.list.push Employee.new(name, birtdate, phone, email, passport, speciality, adress)
            else
                prevWorkName = row["prevWorkName"]
                prevWorkSalary = row["prevWorkSalary"]
                self.listEmp.list.push emp = Employee.new(name, birtdate, phone, email, passport, speciality, adress, prevWorkName, prevWorkSalary)
            end
        end
    end

    def fileDump(filename)
        # Разбиваются по строкам, так как нет символа-сепаратора, недоступного во всех
        # полях поэтому бьем по новым строкам

        crp = Crypto.new
        crp.exportKeyToFile("key", "iv")

        File.open(filename, 'w') do
            |file|

            self.listEmp.list.each do
                |emp|

                file.write(emp.name + "\n")
                file.write(emp.birthdate + "\n")
                file.write(emp.phone + "\n")
                file.write(emp.email + "\n")
                file.write(crp.encrypt(emp.passport) + "\n")
                file.write(emp.speciality + "\n")
                file.write(emp.adress + "\n")
                file.write(emp.experience.to_s + "\n")

                file.write(emp.prevWorkName + "\n") if emp.experience.to_i != 0
                file.write(emp.prevWorkSalary.to_s + "\n") if emp.experience.to_i != 0
            end
        end
    end

    def fileLoad(filename)
        self.listEmp.list.clear
        lines = File.open(filename, 'r').readlines.map { |line| line.chomp }
    
        crp = Crypto.new
        crp.importKeyFromFile("key", "iv")

        while !lines.empty?
            name = lines.delete_at 0
            birthdate = lines.delete_at 0
            phone = lines.delete_at 0
            email = lines.delete_at 0
            passport = crp.decrypt(lines.delete_at(0))
            speciality = lines.delete_at 0
            adress = lines.delete_at 0
            experience = (lines.delete_at 0).to_i

            if experience != 0
                prevWorkName = lines.delete_at 0
                prevWorkSalary = (lines.delete_at 0).to_i
                self.listEmp.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress, experience, prevWorkName, prevWorkSalary)
            else
                self.listEmp.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress)
            end
        end
    end

    def consoleInput
        self.listEmp.consoleInput
    end

    def show
        self.listEmp.show
    end

    def find
        puts <<~"END"
        Поиск по:
        1. ФИО
        2. Email
        3. Красный советский документ
        4. Номер телефона
        END

        print "Выбор: "
        
        case STDIN.gets.chomp.to_i
            when 1
                print "Имя: "
                begin
                    name = STDIN.gets.chomp
                    name = Employee.prettyName(name)
                    res = listEmp.findByName(name)
                    res.show
                rescue RuntimeError => e
                    puts e.message
                end

            when 2
                print "Адрес: "
                begin
                    email = STDIN.gets.chomp
                    email = Employee.prettyEmail(email)
                    res = listEmp.findByEmail(email)
                    res.show
                rescue RuntimeError => e
                    puts e.message
                end

        when 3
            print "Серия и номер: "
            begin
                passport = STDIN.gets.chomp
                passport = Employee.prettyPassport(passport)
                res = listEmp.findByPassport(passport)
                res.show
            rescue RuntimeError => e
                puts e.message
            end

        when 4
            print "Номер: "
            begin
                number = STDIN.gets.chomp
                number = Employee.prettyPhoneNumber(number)
                res = listEmp.findByPhoneNumber(number)
                res.show
            rescue RuntimeError => e
                puts e.message
            end
        end
    end

    def delete(id)
        self.listEmp.list.delete_at id
    end

    def edit(id)
        puts "Редактирование пользователя с ID = #{id}"
        message = "1 - имя, 2 - дата рождения, 3 - телефон, 4 - почта, 5 - пасспорт, 6 - специальность, 7 - адрес"
        puts message
        print "Выбор: "
        ch = STDIN.gets.chomp

        while !ch.empty?
            el = ch.to_i

            begin
                case el
                    when 1
                        self.listEmp.list[id].name = STDIN.gets.strip()

                    when 2
                        self.listEmp.list[id].birthdate = STDIN.gets.strip()

                    when 3
                        self.listEmp.list[id].phone = STDIN.gets.strip()

                    when 4
                        self.listEmp.list[id].email = STDIN.gets.strip()

                    when 5
                        self.listEmp.list[id].passport = STDIN.gets.strip()

                    when 6
                        self.listEmp.list[id].specialities = STDIN.gets.strip()

                    when 7
                        self.listEmp.list[id].adress = STDIN.gets.strip()
                end

            rescue Exception => e
                puts "Данные введены неверно, проверьте ввод. #{e}"
            end

            print "Выбор: "
            ch = STDIN.gets.chomp
        end
    end

    def sort
        message = "1 - имя, 2 - телефон"
        puts message
        print "Выбор: "
        ch = STDIN.gets.chomp

        while !ch.empty?
            el = ch.to_i

            case el
                when 1
                    self.listEmp.sortByName
                when 2
                    self.listEmp.sortByPhone
            end

            print "Выбор: "
            ch = STDIN.gets.chomp
        end
    end
end

terminal = TerminalViewListEmployee.new
terminal.show
