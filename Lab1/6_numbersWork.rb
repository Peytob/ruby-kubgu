def getDigitsSum(integer)
    integer.abs.digits.inject{ |acc, digit| acc + digit }
end

def getMaximalDigit(integer)
    integer.abs.digits.max
end

def getMinimalDigit(integer)
    integer.abs.digits.min
end

def getDigitsMultiplication(integer)
    integer.abs.digits.inject(1) { |acc, digit| acc *= digit }
end

if ARGV.length > 0
    if ARGV[0].strip[/^-?\d+$/]
        number = ARGV[0].to_i
        puts "Сумма цифр числа #{ARGV[0]} равняется #{getDigitsSum(number)}"
        puts "Максимальная цифра в #{ARGV[0]} равняется #{getMaximalDigit(number)}"
        puts "Минимальная цифра в #{ARGV[0]} равняется #{getMinimalDigit(number)}"
        puts "Произведение цифр в #{ARGV[0]} равняется #{getDigitsMultiplication(number)}"
    else
        puts "Кхм. Аргумент должен быть числом!"
    end
else
    puts "Нет заданного числа - нет и его цифр"
end
