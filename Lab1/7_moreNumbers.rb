def getDigitsSum(integer)
    integer.abs.digits.inject{ |acc, digit| acc + digit }
end

def getMaximalDigit(integer)
    integer.abs.digits.max
end

def getMinimalDigit(integer)
    integer.abs.digits.min
end

def getDigitsMultiplication(integer)
    integer.abs.digits.inject(1) { |acc, digit| acc *= digit }
end

# Как я понимаю, использовать require 'prime' нельзя
def isPrime?(integer)
    return false if integer <= 1

    for n in 2..Math.sqrt(integer)
        return false if integer % n == 0
    end

    true
end

def sumOfNotPrimeDividers(integer)
    acc = 0
    for i in 4 .. integer / 2
        acc += integer % i == 0 && !isPrime?(i) ? i : 0
    end
    acc
end

def getDigitsLess3(integer)
    integer.abs.digits.filter{ |digit| digit < 3 }.length
end

def getSomething(integer)
    primeNumbersSum = integer.abs.digits.filter{ |digit| isPrime? digit }.inject{ |acc, d| acc += d }
    primeNumbersSum = primeNumbersSum ? primeNumbersSum : 0
    acc = 0

    puts "primeNumbersSum: #{primeNumbersSum}"

    for i in 2 .. integer
        puts "Результаты для #{i}: #{integer % i} != 0 && #{integer.gcd(i)} != 1 && #{i.gcd(primeNumbersSum)} == 1"
        if integer % i != 0 && integer.gcd(i) != 1 && i.gcd(primeNumbersSum) == 1
            acc += 1
            puts "Найдено: #{i}"
        end
    end

    puts "======================="

    acc
end

if ARGV.length > 0
    if ARGV[0].strip[/^-?\d+$/]
        number = ARGV[0].to_i
        
        puts <<~"END"
        getDigitsSum: #{getDigitsSum(number)}
        getMaximalDigit: #{getMaximalDigit(number)}
        getMinimalDigit: #{getMinimalDigit(number)}
        getDigitsMultiplication: #{getDigitsMultiplication(number)}
        sumOfNotPrimeDividers: #{sumOfNotPrimeDividers(number)}
        getDigitsLess3: #{getDigitsLess3(number)}
        getSomething: #{getSomething(number)}
        END
    else
        puts "Кхм. Аргумент должен быть числом"
    end
else
    puts "Хм. Явно чего-то нехватает"
end
