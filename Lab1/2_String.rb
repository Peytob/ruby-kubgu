if ARGV.length >= 1
    puts "Привет, #{ARGV[0]}!\n"
else
    puts "Привет, незнакомец!\n"
end

# === === Просмотр всех методов класса String === ===

print "Вывести список всех методов класса String? [YES]: "
if STDIN.gets.chomp.downcase.strip === "yes"
    String.methods.each { |method| puts method }

    print "Для продолжения и очистки терминала нажмите Enter"
    STDIN.gets
    Gem.win_platform? ? (system "cls") : (system "clear")
end

# === === Рассмотрение половины методов String === ===

puts <<~"END"
ascii_only?: true, если внутри строки только ascii символы
Some text 123 => #{"Some text 123".ascii_only?} ; Некоторый текст 123 => #{"Некоторый текст 123".ascii_only?}

bytes: конвертирует строку в массив битов
ABC abc 123 => #{"ABC abc 123".bytes}

capitalize: конвертирует первый символ строки в верхний регистр, а остальные в нижний
Some text => #{"Some text".capitalize}

capitalize!: аналогичен capitalize, но возвращает nil, если не было изменений в строке
SomE teXt => #{"SomE teXt".capitalize!} ; Some text => #{"Some text".capitalize! === nil ? "Nil" : "Some text".capitalize!}

center(width, padstr=\"\"): размещает исходную строку в центр строки размера width, заполняя недостающие символы padstr
\"hello\".center(20, '123') => #{"hello".center(20, '123')}

chars: переводит строку в массив char
\"Some cool string\".chars => #{"Some cool string".chars}

codepoints: возвращает массив, содержащий все элементы строки в int.
\"Some cool string\".codepoints => #{"Some cool string".codepoints}

concat: конкатенирует строку с данным массивом
\"ABC\".concat(100, \"text\", \"mur\") => #{"ABC".concat(100, "text", "mur")}

count: Считает сколько раз встречаются данные символы в строке..
\"Some cool string\".count(\"so\") => #{"Some cool string".count("so")}

crypt: Применяет стандартное криптографическое хеширование языка C на строку. Salt_str – «соль».
\"Some cool string\".crypt(\"so\") => #{"Some cool string".crypt("so")}

downcase: Возвращает копию строки, у которой все символы переведены в нижний регистр. Возможна передача некоторых параметров для некоторых языковых групп.
\"Some cool string\".downcase => #{"Some cool string".downcase}

dump: Возвращает копию строки, в которой все непечатаемые символы отображены в виде кодов.
\"Some cool string\".dump => #{"Some cool string".dump}

eql: Возвращает true, если строки одинаковы по длине, содержимому и кодировке.
\"Some cool string\".eql?(\"123\") => #{"Some cool string".eql? 'sdgfdsg'}

hash: возвращает хеш строки.
\"Some cool string\".hash => #{"Some cool string".hash}\n

Include?: Возвращает true, если строка содержит подстроку str
\"Some cool string\".include?('ool') => #{"Some cool string".include?('ool')}

insert: Вставляет строку str, начиная с index
\"Some cool string\".insert(2, 'ool') => #{"Some cool string".insert(2, 'ool')}

lines: Возвращает массив строк строки, если она разделена при помощи разделителя \$.
\"One\\nTwo\\nThree\\n\".lines => #{"One\nTwo\nThree\n".lines}

reverse: Возвращает перевернутую строку
\"Some cool string\".reverse => #{"Some cool string".reverse}

reverse: Возвращает перевернутую строку
\"Some cool string\".reverse => #{"Some cool string".reverse}\n
END

puts ""
print "Для продолжения и очистки терминала нажмите Enter"
STDIN.gets
Gem.win_platform? ? (system "cls") : (system "clear")

# === === Встроенные переменные === ===

print "Вывести список всех встроенных переменных? [YES]: "
if STDIN.gets.chomp.downcase.strip === "yes"
    global_variables.each { |variable| puts variable }
    print "Для продолжения и очистки терминала нажмите Enter"
    STDIN.gets
    Gem.win_platform? ? (system "cls") : (system "clear")
end

puts "Использование форматирования строк через % совместно с некоторыми встроенными переменными"
puts ""

puts "Содержимое переменной ENV: "
ENV.each_pair { |name, value| printf "\t%s : %s\n", name, value }


puts "Содержимое переменной $LOAD_PATH"
$LOAD_PATH.each { |patch| puts ("\t%s" % patch.chomp) }
puts ""

puts "Содержимое переменной $LOADED_FEATURES"
$LOADED_FEATURES.each { |feature| puts "\t%s" % feature.chomp }
puts ""

puts ("Режим отладки:     %d" % ($DEBUG ? 1 : 0))
puts ("PID процесса Ruby: %d" % $$)
puts ("Версия Ruby:       %s" % RUBY_VERSION)
puts ("Дата релиза:       %s" % RUBY_RELEASE_DATE)
puts ("Платформа:         %s" % RUBY_PLATFORM)
