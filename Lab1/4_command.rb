print "Введите команду вашей операционной системы ч/з system (только не надо rm -rf /): "
system STDIN.gets # syscall еще есть, но там иная песня
print "Введите команду Ruby для исполнения: "
eval STDIN.gets

# Иные способы

print "Введите команду вашей операционной системы ч/з `str` (только не надо rm -rf /): "
command = STDIN.gets # syscall еще есть, но там иная песня
consoleOut = `#{command}`
puts "Содержимое вывода команды:\n#{consoleOut}"

print "Вывести список всех встроенных переменных? [YES]: "
if STDIN.gets.chomp.downcase.strip === "yes"
    global_variables.each { |variable| puts variable }
    print "Для продолжения и очистки терминала нажмите Enter"
    STDIN.gets
    Gem.win_platform? ? (system "cls") : (system "clear")
end
