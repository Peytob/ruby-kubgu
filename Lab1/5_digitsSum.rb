if ARGV.length > 0
    if ARGV[0].strip[/^-?\d+$/]
        sum = ARGV[0].to_i.abs.digits.inject{ |acc, digit| acc + digit }
        puts "Сумма цифр числа #{ARGV[0]} равняется #{sum}"
    else
        puts "Кхм. Аргумент должен быть числом!"
    end
else
    puts "Нет заданного числа - нет и его цифр"
end
