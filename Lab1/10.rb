def readAArray(filename, encoding="UTF-8")
    return Array.New if !File.exist?(filename)
    file = File.new(filename, "r:#{encoding}")
    file.readlines[0].split(",\"").map{ |i| i.strip.chomp("\"") }.filter{ |i| !i.empty? }.sort()
end

def score(name)
    name.downcase.each_codepoint.inject(0) { |s, l| s + l.ord - ?a.ord + 1 }
end

# 22
# Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order.
# Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.
# For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
# What is the total of all the name scores in the file?
def case_22()
    humans = readAArray("p022_names.txt")
    scores = humans.filter_map.with_index { |name, index| score(name) * index }.inject(0) { |sum, score| sum + score }
    puts "Total score: #{scores}"
    puts "Colin score: #{score("COLIN")}"
end

# 42
# The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:
# 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 11 / 2 * 11 * (11 + 1) ...
# By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.
# Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?

def isTriangle(t)
    tn = 1 # выразить было бы умнее, ну да ладно. а еще есть рекурсия: n^2 - t(n-1)
    n = 1
    while (tn < t) do
        tn = n * (n + 1) / 2
        n += 1
    end
    t == tn
end

def case_42()
    words = readAArray("p022_names.txt")
    words.count { |word| isTriangle(score(word)) }
end

case_22()
puts "Количество треугольных чисел: #{case_42()}"
