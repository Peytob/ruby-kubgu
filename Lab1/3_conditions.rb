if ARGV.length >= 1
    puts "Привет, #{ARGV[0]}!\n"
else
    puts "Привет, незнакомец!\n"
end

print "Введите ваш любимый язык: "
case STDIN.gets.chomp.downcase
    when "c++"
        puts "Уважаемый язык, молодец! Но Ruby должен быть любимым!"

    when "ruby"
        puts "Подлиза. Зачета не получите, вам на пересдачу!"

    when "python"
        puts "Таких в нашем клубе не любят. Переключайтесь на Ruby!"

    when "pascal"
        puts "Очень пожилой язык. Ruby лучше!"

    else
        puts "Хм. В любом случае, ruby скоро станет вашим любимым языком!"

end

# Первый способ написания условного оператора: if - elsif - else. Стандартный оператор.
# Записывается в несколько строк. Коммит #1 от 15.02.21 0:35

testVar = 52;

if testVar == 41
    puts "Ветвь if - true. testVar = 41"

elsif testVar == 52
    puts "Ветвь if - elsif. testVar = 52"

else
    puts "Ветвь if - false. testVar не угадана, но равна #{testVar}"

end

# Второй способ написания условного оператора: unless - else.
# Коммит #2 от 15.02.21 9:45

unless testVar == 41
    puts "Ветвь unless. testvar =/= 41"

else
    puts "Ветвь else - unless. testvar == 41"

end

# Оставшиеся способы написания условного оператора.

puts "6 > 9" if 6 > 9
puts "6 < 9" if 6 < 9
if 10 > 15 then puts "10 > 15" else puts "10 < 15" end

puts "unless 6 > 9" unless 6 > 9
puts "unless 6 < 9" unless 6 < 9
unless 10 > 15 then puts "unless 10 > 15" else puts "unless 10 < 15" end

# Интересная штука

testVar2 = case testVar
                when 10..15
                    1
                when 50..80
                    2
                else
                    3
                end

puts "testVar2 = #{testVar2}"
