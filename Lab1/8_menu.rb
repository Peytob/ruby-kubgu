def getDigitsSum(integer)
    integer.abs.digits.inject{ |acc, digit| acc + digit }
end

def getMaximalDigit(integer)
    integer.abs.digits.max
end

def getMinimalDigit(integer)
    integer.abs.digits.min
end

def getDigitsMultiplication(integer)
    integer.abs.digits.inject{ |acc, digit| acc *= digit }
end

# Как я понимаю, использовать require 'prime' нельзя
def isPrime?(integer)
    return false if integer <= 1

    for n in 2..Math.sqrt(integer)
        return false if integer % n == 0
    end

    true
  end

def sumOfNotPrimeDividers(integer)
    acc = 0
    for i in 4 .. integer / 2
        acc += integer % i == 0 && !isPrime?(i) ? i : 0
    end
    acc
end

def getDigitsLess3(integer)
    integer.abs.digits.filter{ |digit| digit < 3 }.length
end

def getSomething(integer)
    primeNumbersSum = integer.abs.digits.filter{ |digit| isPrime? digit }.inject{ |acc, d| acc += d }
    primeNumbersSum = primeNumbersSum ? primeNumbersSum : 0
    acc = 0

    for i in 2 .. integer
        if integer % i != 0 && integer.gcd(i) != 1 && i.gcd(primeNumbersSum) == 1
            acc += 1
        end
    end

    acc
end

# Вывод help

if ARGV.include?("-h") || ARGV.include?("--help")
    puts <<~"END"
    Справка.
    Первый аргумент программы - число для анализа, послежующие - необходимые функции. На вход программе подается не менее 2 аргументов - число и функция.
    Список функций и их номеров:
    1 - Сумма чисел числа
    2 - Максимальная цифра числа
    3 - Минимальная цифра числа
    4 - Произведение цифр числа
    5 - Сумма непростых делителей числа:
    6 - Количество цифр, меньших трех
    7 - количество чисел, не являющихся делителями исходного числа, не взамно простых с ним и взаимно простых с суммой простых цифр этого числа
    END

    ARGV.delete "-h"
    ARGV.delete "--help"
end

# Проверка ввода

abort "Hello world!" if ARGV.length == 0
abort "Недостаточно аргументов: необходимо не менее двух! (см. -h для помощи)" if ARGV.length < 2
ARGV.each do
    |function|
    abort "Проверьте ввод данных: #{function} не целое число (см. -h для помощи)" unless function[/^-?\d+$/]
end

# Выборка

choose = {
    1 => lambda { |var| puts "Сумма чисел числа #{var}: #{getDigitsSum(var)}" },
    2 => lambda { |var| puts "Максимальная цифра числа #{var}: #{getMaximalDigit(var)}" },
    3 => lambda { |var| puts "Минимальная цифра числа #{var}: #{getMinimalDigit(var)}" },
    4 => lambda { |var| puts "Произведение цифр числа #{var}: #{getDigitsMultiplication(var)}" },
    5 => lambda { |var| puts "Сумма непростых делителей числа: #{var}: #{sumOfNotPrimeDividers(var)}" },
    6 => lambda { |var| puts "Количество цифр, меньших трех: #{var}: #{getDigitsLess3(var)}" },
    7 => lambda { |var| puts "количество чисел, не являющихся делителями исходного " +
        "числа, не взамно простых с ним и взаимно простых с суммой простых цифр этого числа #{var}: #{getSomething(var)}" },
}
choose.default = lambda { |var| puts "Хм. Я не знаю такой опции..." }

ARGV[1 .. -1].each { |i| choose[i.to_i][ARGV[0].to_i] }
