class Salary
    attr_accessor :fixed, :quartelyPercent, :possibleBonusPercent

    def initialize(fixed, quartelyPercent, fixedPremium, possibleBonusPercent)
        self.fixed = fixed
        self.fixedPremium = fixedPremium
        self.quartelyPercent = quartelyPercent
        self.possibleBonusPercent = possibleBonusPercent
    end

    def get(mon)
        self.fixed
    end

    def getWithPossibleBonus(mon)
        self.fixed + self.fixed * self.possibleBonusPercent * 0.25
    end

    def getWithPremium(mon)
        self.fixed + self.fixedPremium
    end

    def getWithQurtely(mon)
        self.fixed + self.fixed * self.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
    end
end

# Вопрос: почему нельзя сделать примерно так:
# salary = JustSalary.new(QuartelySalary.new(OtherSalaryType.new ... ))
# ? ? ?
# Таким образом накручиваю ниже, используя лишь 3 класса. И выходит заметно удобнее.

class SalaryDecorator
    attr_accessor :target
    def initialize(target)
        self.target = target
    end

    def get(mon)
        raise NotImplementedError
    end
end

# Вариант с просто фиксированным окладом
class JustSalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        target.get(mon)
    end
end

# Вариант с фиксированным окладом и фиксированными премиальными
class FixedPremiumSalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        target.getWithPremium(mon)
    end
end

# Вариант с ежеквартальными выплатами
class QuartelySalary
    def initialize(target)
        target
    end

    def get(mon)
        target.getWithQurtely(mon)
    end
end

# Вариант с фиксированным окладом и фиксированными премиальными с вкусняшками каждый квартал
class FixedPremiumQuartelySalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        total = target.getWithPremium
        total + total * target.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
    end
end

# Вариант с возможным окладом
class RandomBonusSalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        target.getWithPossibleBonus(mon)
    end
end

# Вариант с возможным окладом и обязательными премиальными
class RandomBonusFixedSalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        target.getWithPossibleBonus(mon) + target.fixedPremium
    end
end

# Вариант с возможным окладом и ежеквартальными
class RandomBonusQuartelySalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        total = target.getWithPossibleBonus(mon)
        total + total * target.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
    end
end

# Дорогой богатый вариант со всеми возможными надбавками
class BurgeoisSalary < SalaryDecorator
    def initialize(target)
        super(target)
    end

    def get(mon)
        total = target.getWithPossibleBonus(mon) + target.fixed
        total + total * target.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
    end
end

# ==============================================================================

class Post
    attr_accessor :salary, :employee, :name, :departament

    # row - из базы данных строка / просто хеш. Паттерн builder, можно сказать
    def initialize(row)
        self.employee = nil
        self.name = row["name"]

        salary = Salary.new(row["fixedSalary"], row["quarterlyAwardSize"], row["fixedPremiumSize"], row["possibleBonusPercent"])
        salary = FixedPremiumSalary(salary) if row["fixedPremiumBool"]
        salary = RandomBonusSalary(salary) if row["possibleBonusBool"]
        salary = QuartelySalary(salary) if row["quarterlyAwardBool"]
    end
end

# =============================================================================

class Departament
end

# =============================================================================

class PostList
private

    attr_accessor :posts

public

    def initialize
        self.posts = Array.new
    end

    def add(post)
        raise ArgumentError.new("Post should be an object of Post class.") if post.class != Post
        self.posts << post
    end

    def choose(id)
        self.posts.find { |post| post.id == id }
    end
end
