require_relative 'InputCorrector.rb'

class Contract
    attr_accessor :type, :tariffRate,
        :insuranceAmount, :philial, :date, :dbid

    def initialize(type, date, philial, tariffRate, insuranceAmount, dbid = -1)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
        self.dbid = dbid
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{self.philial.title} (тарифная ставка: #{tariffRate}; страховая сумма: #{self.insuranceAmount})"
    end

    def date=(date)
        @date = InputCorrector.prettyDate(date)
    end

    def date
        @date
    end

    def philial=(philial)
        raise TypeError.new("Philial field should be Philial class") if philial.class != Philial

        @philial = philial
    end

    def philial
        @philial
    end
end
