class Philial
    attr_accessor :title, :adress
    attr_reader :databaseId

    def initialize(title, adress, phone, id = -1)
        self.title = title
        self.adress = adress
        self.phone = phone
        @databaseId = id
    end

    def to_s
        "Филиал: #{self.title} по адресу #{self.adress} (телефон: #{self.phone})"
    end

    def phone=(phone)
        @phone = InputCorrector.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end
end
