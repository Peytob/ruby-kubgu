class InputCorrector
    def self.isPhoneNumberCorrect?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrect? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def self.isDateCorrect?(date)
        date =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyDate(date)
        raise "Неверный формат даты: #{date}" unless isDateCorrect? date.strip
        date = date.split('.').map {|i| i.to_i.to_s }
        date[0] = "0" + date[0] if date[0].to_i < 10
        date[1] = "0" + date[1] if date[1].to_i < 10
        date[2] = "20" + date[2] if date[2].to_i < 100
        date.join(".")
    end
end
