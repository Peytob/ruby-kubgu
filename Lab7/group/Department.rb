require "./PostList.rb"

class Department
    attr_accessor :id, :name, :posts
    def initialize(id, name)
        self.id = id
        self.name = name
        self.posts = PostList.new
        self.posts.department = self
    end

    def readDB
        self.posts.readDB
    end

    def id=(newId)
        raise "Department id is #{self.id}!" if self.id
        @id = newId
    end

    def to_json
        {
            "id" => self.id,
            "name" => self.name
        }.to_json
    end
end
