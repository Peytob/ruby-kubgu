require "json"
require_relative "ReadStrategy"

class PostList
protected

    attr_accessor :posts, :strategy

public

    attr_accessor :department

    def initialize
        self.posts = Array.new
        self.strategy = ReadStrategy::ReadDBAll.new(self)
    end

    def add(post)
        raise ArgumentError.new("Post should be an object of Post class.") if post.class != Post
        self.posts << post
        DatabaseWork.getInstance.addPost(post)
    end

    def edit(post)
        DatabaseWork.getInstance.updatePost(post)
    end

    def delete(post)
        DatabaseWork.getInstance.deletePost(post)
    end

    def choose(id)
        self.posts.find { |post| post.id == id }
    end

    def readDB
        self.strategy.read
    end

    def department=(dep)
        self.strategy = dep ? ReadStrategy::ReadDBDep.new(self) : ReadStrategy::ReadDBAll.new(self)
        @department = dep
    end

    def to_json
        posts = []
        self.posts.each do
            |post|

            posts.push JSON.parse(post.to_json)
        end

        {
            "posts" => posts
        }.to_json
    end
end
