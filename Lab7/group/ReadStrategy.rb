require_relative "DatabaseWork.rb"
require_relative "Post.rb"

module ReadStrategy
    class ReadingDBStarategy
        attr_accessor :target

        def initialize(target)
            self.target = target
        end

        def read
            raise NotImplementedError.new("Method read is not emplemented!")
        end
    end

    # Чтение всех должностей
    class ReadDBAll < ReadingDBStarategy
        def initialize(target)
            super(target)
        end

        def read
            DatabaseWork.getInstance().selectAllPosts(target)
        end
    end

    # Чтение должностей только департамента
    class ReadDBDep < ReadingDBStarategy
        def initialize(target)
            super(target)
        end

        def read
            DatabaseWork.getInstance().selectAllPostsFor(target, self.target.department)
        end
    end
end
