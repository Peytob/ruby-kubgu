require 'date'
require 'yaml'
require 'json'
require 'nokogiri'

require './Employee.rb'

class EmployeeList
    attr_accessor :list

    def initialize
        self.list = Array.new
    end

    def findByName(value)
        findLst = EmployeeList.new
        findLst.list = self.list.filter { |emp| emp.name === value }
        findLst
    end

    def findByEmail(value)
        findLst = EmployeeList.new
        findLst.list = self.list.filter { |emp| emp.email === value }
        findLst
    end

    def findByPassport(value)
        findLst = EmployeeList.new
        findLst.list = self.list.filter { |emp| emp.passport === value }
        findLst
    end

    def findByPhoneNumber(value)
        findLst = EmployeeList.new
        findLst.list = self.list.filter { |emp| emp.phone === value }
        findLst
    end

    def sortByName()
        self.list.sort_by! { |emp| emp.name }
    end

    def sortByPhone()
        self.list.sort_by! { |emp| emp.phone }
    end

    def writeListYAML(filename)
        File.open(filename, "w") do
            |file|
            YAML.dump(self.list, file)
        end
    end

    def readListYAML(filename)
        self.list = YAML.load_file(filename)
    end

    def to_json
        jsonList = self.list.map { |i| i.to_json }.join(", ")
        "[" + jsonList + "]"
        # Почему оно переводит массив в что-то типа {["{}", "{}", ...]} ???
    end

    def writeListJSON(filename)
        File.open(filename, "w") do
            |file|
            file.write(self.to_json) # Переводит emp в строку
        end
    end

    def readListJSON(filename)
        json = ""
        File.open(filename, 'r') do
            |file|

            json = file.read
        end

        JSON.parse(json).each do
            |empJson|

            emp = Employee.new(empJson)
            self.list.push emp
        end
    end

    def writeListXML(filename)
        doc = Nokogiri::XML "<employees></employees>"
        root = doc.at_css("employees")

        self.list.each { |emp|  root.add_child (emp.to_xmlNode doc) }

        File.open(filename, 'w') do
            |file|

            file.write(doc)
        end
    end

    def readListXML(filename)
        doc = File.open(filename, 'r').read
        doc = Nokogiri::XML doc

        return if !doc.errors.empty?

        # puts doc

        doc.css("employee").map do
            |node|

            builder = {
                "name" => node.css("name").text
                "birthdate" => node.css("birthdate").text
                "phone" => node.css("phone").text
                "email" => node.css("email").text
                "passport" => node.css("passport").text
                "speciality" => node.css("speciality").text
                "adress" => node.css("adress").text
                "experience" => node.css("experience").text
                "prevWorkName" => node.css("prevWorkName").text
                "prevWorkSalary" => node.css("prevWorkSalary").text
            }

            self.list.push Employee.new(builder)
        end
    end

    def add(emp)
        self.list << emp
    end
end
