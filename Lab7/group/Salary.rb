class Salary
    attr_accessor :fixed, :quartelyPercent, :possibleBonusPercent

    def initialize(fixed, quartelyPercent, fixedPremium, possibleBonusPercent)
        self.fixed = fixed
        self.fixedPremium = fixedPremium
        self.quartelyPercent = quartelyPercent
        self.possibleBonusPercent = possibleBonusPercent
    end

    def get(mon)
        self.fixed
    end

    def getWithPossibleBonus(mon)
        self.fixed + self.fixed * self.possibleBonusPercent * 0.25
    end

    def getWithPremium(mon)
        self.fixed + self.fixedPremium
    end

    def getWithQurtely(mon)
        self.fixed + self.fixed * self.quartelyPercent * (mon % 3 == 0 ? 1 : 0)
    end
end
