class InputCorrector
    def self.isPhoneNumberCorrect?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrect? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def self.isDateCorrect?(date)
        date =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyDate(date)
        raise "Неверный формат даты: #{date}" unless isDateCorrect? date.strip
        date = date.split('.').map {|i| i.to_i.to_s }
        date[0] = "0" + date[0] if date[0].to_i < 10
        date[1] = "0" + date[1] if date[1].to_i < 10
        date[2] = "20" + date[2] if date[2].to_i < 100
        date.join(".")
    end
end

class Philial
    attr_accessor :title, :adress

    def initialize(title, adress, phone)
        self.title = title
        self.adress = adress
        self.phone = phone
    end

    def to_s
        "Филиал: #{self.title} по адресу #{self.adress} (телефон: #{self.phone})"
    end

    def phone=(phone)
        @phone = InputCorrector.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end
end

class Contract
    attr_accessor :type, :tariffRate,
        :insuranceAmount

    def initialize(type, date, philial, tariffRate, insuranceAmount)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{self.philial.title} (тарифная ставка: #{tariffRate}; страховая сумма: #{self.insuranceAmount})"
    end

    def date=(date)
        @date = InputCorrector.prettyDate(date)
    end

    def date
        @date
    end

    def philial=(philial)
        raise TypeError.new("Philial field should be Philial class") if philial.class != Philial

        @philial = philial
    end

    def philial
        @philial
    end
end

class TestContract
private
    @@dates = ["1.1.1978", "5.2.2004", "11.12.1921", "15.6.2001", "31.2.1965", "21.4.1971", "25.9.1972", "30.10.1985"]
    @@phones = ["+79955347123", "+79184325434", "+79182134954", "+79182353764", "+79953457234", "+79912417635"]
    @@adresses = ["Кранснодар, Ул. уличная, 124 кв 87", "Кранснодар, Ул. Петрова, 51 кв 44", "Кранснодар, Ул. Иванова, 63 кв 16"]
    @@philialTitle = ["Расгосстрах", "Страховочка", "Выплаты"]
    @@type = ["Cтрахование автотранспорта от угона", "Cтрахование домашнего имущества", "Добровольное медицинское страхование"]

public
    def self.generate(philials, contracts)
        5.downto(1) { |i| philials << Philial.new(@@philialTitle.sample, @@adresses.sample, @@phones.sample) }
        10.downto(1) { |i| contracts << Contract.new(@@type.sample, @@dates.sample, philials.sample, 1000 + rand(6000), 1000 + rand(6000)) }
    end
end

class TestInput
    def self.check(option, data)
        optionsTable = {
            "phone" => lambda { |var| InputCorrector.prettyPhoneNumber(var) },
            "date" => lambda { |var| InputCorrector.prettyDate(var) }
        }
        optionsTable.default = lambda { |var| puts "Хм. Я не знаю такой опции..." }

        begin
            res = optionsTable[option][data]
        rescue RuntimeError => e
            res = "Ошибка - данные введены неверно..."
        end

        res
    end
end

class ImpotantDatabase
    attr_accessor :philials, :contracts

    def initialize(fill = false)
        self.philials = Array.new
        self.contracts = Array.new

        TestContract.generate(self.philials, self.contracts) if fill
    end

    def show()
        puts "=== ФИЛИАЛЫ ==="
        self.philials.filter_map.with_index  { |e, i| puts "#{i + 1} : #{e}" }
        puts "=== КОНТРАКТЫ ==="
        self.contracts.filter_map.with_index { |e, i| puts "#{i + 1} : #{e}" }
    end

    def input()
        print "Введите тип: "
        type = STDIN.gets.chomp.strip

        print "Введите дату: "
        date = STDIN.gets.chomp.strip

        print "Введите номер филиала (см show): "
        philial = STDIN.gets.chomp.strip.to_i

        print "Введите тарифную ставку: "
        tariffRate = STDIN.gets.chomp.strip.to_i

        print "Введите размер выплаты: "
        insuranceAmount = STDIN.gets.chomp.strip.to_i

        self.contracts.push Contract.new(type.chomp, date.chomp, self.philials[philial], tariffRate, insuranceAmount)
    end

    def findByDate
        print "Введите дату: "
        date = STDIN.gets.chomp.strip
        date = InputCorrector.prettyDate(date)

        self.contracts.each_with_index { |e, i| puts "#{i + 1} : #{e}" if e.date == date }
    end

    def edit(id)
        id = id - 1
        message = "Изменить: 1 - тип, 2 - дату, 3 - тарифную ставку, 4 - размер выплаты "
        print message
        number = STDIN.gets.chomp.strip
        while !number.empty?
            number = number.to_i

            case number
            when 1
                self.contracts[id].type = STDIN.gets.chomp.strip

            when 2
                self.contracts[id].date = STDIN.gets.chomp.strip

            when 3
                self.contracts[id].tariffRate = STDIN.gets.chomp.strip.to_i

            when 4
                self.contracts[id].insuranceAmount = STDIN.gets.chomp.strip.to_i

            else
                puts "Введено что-то не то"
            end

            print message
            number = STDIN.gets.chomp.strip
        end
    end

    def delete(id)
        id = id - 1
        self.contracts.delete_at(id)
    end
end

class TerminalView
    attr_accessor :database

    def initialize(fill = false)
        self.database = ImpotantDatabase.new(fill)
    end

    def show()
        self.database.show()
    end

    def toFile(filename)
        index = 0
        File.open(filename + "-philials", 'w') do
            |file|

            self.database.philials.each do
                |philial|

                file.write(index.to_s + "\n")
                file.write(philial.title + "\n")
                file.write(philial.adress + "\n")
                file.write(philial.phone + "\n")

                index += 1
            end
        end

        File.open(filename + "-contracts", 'w') do
            |file|

            self.database.contracts.each do
                |contract|

                file.write(contract.type + "\n")
                file.write(contract.date + "\n")
                file.write(self.database.philials.index(contract.philial).to_s + "\n")
                file.write(contract.tariffRate.to_s + "\n")
                file.write(contract.insuranceAmount.to_s + "\n")

                index += 1
            end
        end
    end

    def fromFile(filename)
        self.database.philials.clear
        philialsLines = File.open(filename + "-philials", 'r').readlines

        while !philialsLines.empty?
            index = philialsLines.delete_at(0).to_i
            title = philialsLines.delete_at(0)
            adress = philialsLines.delete_at(0)
            phone = philialsLines.delete_at(0)

            self.database.philials[index] = Philial.new(title.chomp, adress.chomp, phone.chomp)
        end

        self.database.contracts.clear
        contractsLines = File.open(filename + "-contracts", 'r').readlines

        while !contractsLines.empty?
            type = contractsLines.delete_at(0)
            date = contractsLines.delete_at(0)
            philial = self.database.philials[contractsLines.delete_at(0).to_i]
            tariffRate = contractsLines.delete_at(0).to_i
            insuranceAmount = contractsLines.delete_at(0).to_i

            self.database.contracts.push Contract.new(type.chomp, date.chomp, philial, tariffRate, insuranceAmount)
        end
    end

    def input
        self.database.input
    end

    def findByDate
        self.database.findByDate
    end

    def edit(id)
        self.database.edit(id)
    end

    def delete(id)
        self.database.delete(id)
    end
    
    def sort
    end
end

terminal = TerminalView.new true

message = "(show - показать, edit - редактировать, add - добавить, dump - выгрузка в файл, load - загрузка из файла, find - поиск): "
print message
command = STDIN.gets.chomp.strip
while !command.empty?
    if command == "show"
        terminal.show

    elsif command == "dump"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        terminal.toFile(filename)

    elsif command == "load"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        terminal.fromFile(filename)

    elsif command == "add"
        begin
            terminal.input
        rescue => exception
            puts "Что-то пошло не так: #{exception}"
        end

    elsif command == "find"
        begin
            terminal.findByDate
        rescue => exception
            puts "Что-то пошло не так: #{exception}"
        end
        
    elsif command == "edit"
        print "Введите номер контракта (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            terminal.edit(id)
        rescue => exception
            puts "Ошибка: #{exception}"
        end

    elsif command == "delete"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            terminal.delete(id)
        rescue => exception
            puts "Ошибка: #{exception}"
        end

    elsif command == "sort"
        terminal.sort

    elsif command == "exit"
        exit 0
    end

    print message
    command = STDIN.gets.chomp.strip
end
