class Philal
    attr_accessor :title, :adress, :phone

    def initialize(title, adress, phone)
        self.title = title
        self.adress = adress
        self.phone = phone
    end

    def to_s
        title
    end
end

class Contract
    attr_accessor :type, :date, :philial, :tariffRate,
        :insuranceAmount

    def initialize(type, date, philial, tariffRate, insuranceAmount)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{title}"
    end
end

