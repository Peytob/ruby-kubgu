class InputCorrector
    def self.isPhoneNumberCorrect?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless isPhoneNumberCorrect? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def self.isDateCorrect?(date)
        date =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyDate(date)
        raise "Неверный формат даты: #{date}" unless isDateCorrect? date.strip
        date = date.split('.').map {|i| i.to_i.to_s }
        date[0] = "0" + date[0] if date[0].to_i < 10
        date[1] = "0" + date[1] if date[1].to_i < 10
        date[2] = "20" + date[2] if date[2].to_i < 100
        date.join(".")
    end
end

class Philial
    attr_accessor :title, :adress

    def initialize(title, adress, phone)
        self.title = title
        self.adress = adress
        self.phone = phone
    end

    def to_s
        "Филиал: #{self.title} по адресу #{self.adress} (телефон: #{self.phone})"
    end

    def phone=(phone)
        @phone = InputCorrector.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end
end

class Contract
    attr_accessor :type, :tariffRate,
        :insuranceAmount

    def initialize(type, date, philial, tariffRate, insuranceAmount)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{self.philial.title} (тарифная ставка: #{tariffRate}; страховая сумма: #{self.insuranceAmount})"
    end

    def date=(date)
        @date = InputCorrector.prettyDate(date)
    end

    def date
        @date
    end

    def philial=(philial)
        raise TypeError.new("Philial field should be Philial class")

        @philial = philial
    end

    def philial
        @philial
    end
end

class TestContract
private
    @@dates = ["1.1.1978", "5.2.2004", "11.12.1921", "15.6.2001", "31.2.1965", "21.4.1971", "25.9.1972", "30.10.1985"]
    @@phones = ["+79955347123", "+79184325434", "+79182134954", "+79182353764", "+79953457234", "+79912417635"]
    @@adresses = ["Кранснодар, Ул. уличная, 124 кв 87", "Кранснодар, Ул. Петрова, 51 кв 44", "Кранснодар, Ул. Иванова, 63 кв 16"]
    @@philialTitle = ["Расгосстрах", "Страховочка", "Выплаты"]
    @@type = ["Cтрахование автотранспорта от угона", "Cтрахование домашнего имущества", "Добровольное медицинское страхование"]

public
    def self.generate(philials, contracts)
        5.downto(1) { |i| philials << Philial.new(@@philialTitle.sample, @@adresses.sample, @@phones.sample) }
        10.downto(1) { |i| contracts << Contract.new(@@type.sample, @@dates.sample, philials.sample, 1000 + rand(6000), 1000 + rand(6000)) }
    end
end

philials = Array.new
contracts = Array.new
TestContract.generate(philials, contracts)

philials.each { |p| puts p }
contracts.each { |c| puts c }
