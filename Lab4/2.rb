class Philal
    attr_accessor :title, :adress, :phone

    def initialize(title, adress, phone)
        self.title = title
        self.adress = adress
        self.phone = phone
    end

    def to_s
        "Филиал: #{self.title} по адресу #{self.adress} (телефон: #{self.phone})"
    end
end

class Contract
    attr_accessor :type, :date, :philial, :tariffRate,
        :insuranceAmount

    def initialize(type, date, philial, tariffRate, insuranceAmount)
        self.type = type
        self.date = date
        self.philial = philial
        self.tariffRate = tariffRate
        self.insuranceAmount = insuranceAmount
    end

    def to_s
        "Контракт: #{self.type} от #{self.date} в филиале #{self.philial.title} (тарифная ставка: #{tariffRate}; страховая сумма: #{self.insuranceAmount})"
    end
end

class TestContract
private
    @@dates = ["01.01.1978", "05.02.2004", "11.12.1921", "15.06.2001", "31.02.1965", "21.04.1971", "25.09.1972", "30.10.1985"]
    @@phones = ["+79955347123", "+79184325434", "+79182134954", "+79182353764", "+79953457234", "+79912417635"]
    @@adresses = ["Кранснодар, Ул. уличная, 124 кв 87", "Кранснодар, Ул. Петрова, 51 кв 44", "Кранснодар, Ул. Иванова, 63 кв 16"]
    @@philialTitle = ["Расгосстрах", "Страховочка", "Выплаты"]
    @@type = ["Cтрахование автотранспорта от угона", "Cтрахование домашнего имущества", "Добровольное медицинское страхование"]

public
    def self.generate(philials, contracts)
        5.downto(1) { |i| philials << Philal.new(@@philialTitle.sample, @@adresses.sample, @@phones.sample) }
        10.downto(1) { |i| contracts << Contract.new(@@type.sample, @@dates.sample, philials.sample, 1000 + rand(6000), 1000 + rand(6000)) }
    end
end

philials = Array.new
contracts = Array.new
TestContract.generate(philials, contracts)

philials.each { |p| puts p }
contracts.each { |c| puts c }
