require './TerminalView.rb'

terminal = TerminalView.new
terminal.show
message = "Введите команду (show - показать, add - добавить, dump - выгрузка в файл, load - загрузка из файла, find - поиск, edit - редактирование по id, delete - удаление по id, sort - сортировка exit - выход): "
print message
command = STDIN.gets.strip

while !command.empty?
    if command == "show"
        terminal.show

    elsif command == "dump"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        terminal.fileDump(filename)

    elsif command == "load"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        terminal.fileLoad(filename)

    elsif command == "add"
        terminal.consoleInput

    elsif command == "find"
        terminal.find

    elsif command == "edit"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            terminal.edit(id)
        rescue
            puts "Айдишка точно является числом?.."
        end

    elsif command == "delete"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            terminal.delete(id)
        rescue
            puts "Айдишка точно является числом?.."
        end

    elsif command == "sort"
        terminal.sort

    elsif command == "exit"
        terminal.toYaml("dump")
        exit 0
    end

    print message
    command = STDIN.gets.strip
end

terminal.dumpYaml "dump.yaml"
