require 'yaml'

class ContractsList
    attr_accessor :philials, :contracts

    def initialize(philials = [], contracts = [])
        self.philials = philials
        self.contracts = contracts
    end

    def show()
        puts "=== ФИЛИАЛЫ ==="
        self.philials.filter_map.with_index  { |e, i| puts "#{i + 1} : #{e}" }
        puts "=== КОНТРАКТЫ ==="
        self.contracts.filter_map.with_index { |e, i| puts "#{i + 1} : #{e}" }
    end

    def input()
        print "Введите тип: "
        type = STDIN.gets.chomp.strip

        print "Введите дату: "
        date = STDIN.gets.chomp.strip

        print "Введите номер филиала (см show): "
        philial = STDIN.gets.chomp.strip.to_i

        print "Введите тарифную ставку: "
        tariffRate = STDIN.gets.chomp.strip.to_i

        print "Введите размер выплаты: "
        insuranceAmount = STDIN.gets.chomp.strip.to_i

        self.contracts.push Contract.new(type.chomp, date.chomp, self.philials[philial], tariffRate, insuranceAmount)
    end

    def findByDate(date)
        date = InputCorrector.prettyDate(date)

        self.contracts.map.with_index { |e, i| [i + 1, e] if e.date == date }.compact
    end

    def findByType(type)
        self.contracts.map.with_index { |e, i| [i + 1, e] if e.type == type }.compact
    end

    def edit(id)
        message = "Изменить: 1 - тип, 2 - дату, 3 - тарифную ставку, 4 - размер выплаты "
        print message
        number = STDIN.gets.chomp.strip
        while !number.empty?
            number = number.to_i

            case number
            when 1
                self.contracts[id].type = STDIN.gets.chomp.strip

            when 2
                self.contracts[id].date = STDIN.gets.chomp.strip

            when 3
                self.contracts[id].tariffRate = STDIN.gets.chomp.strip.to_i

            when 4
                self.contracts[id].insuranceAmount = STDIN.gets.chomp.strip.to_i

            else
                puts "Введено что-то не то"
            end

            print message
            number = STDIN.gets.chomp.strip
        end
    end

    def delete(id)
        id = id - 1
        self.contracts.delete_at(id)
    end

    def sortByDate
        self.contracts = self.contracts.sort_by { |i| i.date }
    end

    def sortByPhilialName
        self.contracts = self.contracts.sort_by { |i| i.philial.title }
    end

    def sortByTariff
        self.contracts = self.contracts.sort_by { |i| i.tariffRate }
    end

    def toYaml(filename)
        File.open(filename + "-philials.yaml", "w") do
            |file|
            YAML.dump(self.philials, file)
        end

        File.open(filename + "-contracts.yaml", "w") do
            |file|
            YAML.dump(self.contracts, file)
        end
    end

    def fromYaml(filename)
        self.philials = YAML.load_file(filename + "-philials.yaml")
        self.contracts = YAML.load_file(filename + "-contracts.yaml")
    end
end
