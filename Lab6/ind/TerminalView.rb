require './Contract.rb'
require './DatabaseConnection.rb'
require './Philial.rb'
require './ContractsList.rb'

class TerminalView
    attr_accessor :database, :connection

    def initialize(fill = false)
        self.connection = DatabaseConnection.new("user12", "34klq*")
        self.database = self.connection.readListDB
        self.database.sortByTariff

        databaseYaml = ContractsList.new
        databaseYaml.sortByTariff

        puts "Данные в БД и дампе не совпали. Взяты данные из БД" if self.database.eql?(databaseYaml)
    end

    def show()
        self.database.show()
    end

    def toFile(filename)
        index = 0
        File.open(filename + "-philials", 'w') do
            |file|

            self.database.philials.each do
                |philial|

                file.write(index.to_s + "\n")
                file.write(philial.title + "\n")
                file.write(philial.adress + "\n")
                file.write(philial.phone + "\n")

                index += 1
            end
        end

        File.open(filename + "-contracts", 'w') do
            |file|

            self.database.contracts.each do
                |contract|

                file.write(contract.type + "\n")
                file.write(contract.date + "\n")
                file.write(self.database.philials.index(contract.philial).to_s + "\n")
                file.write(contract.tariffRate.to_s + "\n")
                file.write(contract.insuranceAmount.to_s + "\n")

                index += 1
            end
        end
    end

    def fromFile(filename)
        self.database.philials.clear
        philialsLines = File.open(filename + "-philials", 'r').readlines

        while !philialsLines.empty?
            index = philialsLines.delete_at(0).to_i
            title = philialsLines.delete_at(0)
            adress = philialsLines.delete_at(0)
            phone = philialsLines.delete_at(0)

            self.database.philials[index] = Philial.new(title.chomp, adress.chomp, phone.chomp)
        end

        self.database.contracts.clear
        contractsLines = File.open(filename + "-contracts", 'r').readlines

        while !contractsLines.empty?
            type = contractsLines.delete_at(0)
            date = contractsLines.delete_at(0)
            philial = self.database.philials[contractsLines.delete_at(0).to_i]
            tariffRate = contractsLines.delete_at(0).to_i
            insuranceAmount = contractsLines.delete_at(0).to_i

            self.database.contracts.push Contract.new(type.chomp, date.chomp, philial, tariffRate, insuranceAmount)
        end
    end

    def input
        self.database.input
        self.connection.add(self.database.contracts[-1])
    end

    def find
        print "Найти по: 1 - дата, 2 - тип"
        number = STDIN.gets.chomp.strip.to_i
        print "Запрос: "
        what = STDIN.gets.chomp.strip
        case number
        when 1
            self.database.findByDate(what).each { |i| puts "#{i[0]} : #{i[1]}" }

        when 2
            self.database.findByType(what).each { |i| puts "#{i[0]} : #{i[1]}" }

        else
            puts "Введено что-то не то"
        end
    end

    def edit(id)
        self.database.edit(id - 1)
        self.connection.add(self.database.contracts[id - 1])
    end

    def delete(id)
        self.connection.delete(self.database.contracts[id - 1])
        self.database.delete(id - 1)
    end
    
    def sort
        print "Сортировать по: 1 - дата, 2 - название филиала, 3 - тарифную ставку "
        number = STDIN.gets.chomp.strip.to_i
        case number
        when 1
            self.database.sortByDate

        when 2
            self.database.sortByPhilialName

        when 3
            self.database.sortByTariff

        else
            puts "Введено что-то не то"
        end
    end
end
