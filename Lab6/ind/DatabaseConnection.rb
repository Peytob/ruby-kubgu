require 'mysql2'
require 'date'

class DatabaseConnection
private
    attr_accessor :connection

public
    def initialize(username, password)
        self.connection = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => "insurance")
        self.connection.query('SELECT VERSION()').each do
            |row|
    
            puts "Версия базы данных: #{row["VERSION()"]}"
        end
    end

    def readListDB
        philials = Array.new

        self.connection.query("SELECT * FROM Philials;").each do
            |row|
            title = row["title"]
            phone = row["phone"]
            adress = row["adress"]
            id = row["id"]

            philials.push << Philial.new(title, adress, phone, id)
        end

        contracts = Array.new
        self.connection.query("SELECT * FROM Contracts;").each do
            |row|
            type = row["type"]
            date = row["date"]
            date = "#{date.mday}.#{date.mon}.#{date.year}"
            philial = philials.find { |ph| ph.databaseId == row["philialId"] }
            tariffRate = row["tariffRate"]
            insuranceAmount = row["insuranceAmount"]

            contracts.push << Contract.new(type, date, philial, tariffRate, insuranceAmount, row["id"])
        end

        ContractsList.new philials, contracts
    end

    def add(contract)
        # insert into `Philials` values(NULL, 'Организация страховок', '+7-918-2353764', 'Яблоновский 666');
        # insert into `Contracts` values(NULL, 'Страхование жизни', '2020-11-27', 5000, 1000000, 2);

        date = Date.strptime(contract.date, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "INSERT INTO `Contracts` values(NULL, '#{contract.type}', '#{date}', #{contract.tariffRate}, #{contract.insuranceAmount}, #{contract.philial.databaseId});"
        self.connection.query(str)
        self.connection.query("LAST_INSERT_ID()") { |id| contract.dbid = id }
    end

    def delete(contract)
        str = "DELETE FROM `Contracts` WHERE type = '#{contract.type}' AND tariffRate = #{contract.tariffRate} AND insuranceAmount = #{contract.insuranceAmount} AND philialId = #{contract.philial.databaseId};"
        self.connection.query(str)
    end

    def edit(contract)
        date = Date.strptime(contract.date, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "UPDATE `Contracts` SET "
        str += "type = '#{contract.type}', "
        str += "date = '#{date}', "
        str += "tariffRate = '#{contract.tariffRate}', "
        str += "insuranceAmount = '#{contract.insuranceAmount}', "
        str += "philialId = '#{contract.philialId}', "
        str += "WHERE id = '#{contract.dbid}';"
    end
end
