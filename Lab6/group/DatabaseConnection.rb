require 'mysql2'

require './Employee.rb'
require './ListEmployee.rb'

class DatabaseConnection
private
    attr_accessor :connection

public
    def initialize(username, password)
        self.connection = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => "stuff")
        self.connection.query('SELECT VERSION()').each do
            |row|
    
            puts "Версия базы данных: #{row["VERSION()"]}"
        end
    end

    def readListDB
        # {"id"=>1, "name"=>"Иванов Иван Иванович", "birtdate"=>#<Date: 2021-03-27 ((2459301j,0s,0n),+0s,2299161j)>,
        # "phone"=>"+7-918-2353764", "email"=>"some@mail.ru", "passport"=>"1234 123456", "speciality"=>"santex",
        # "adress"=>"Krasnodar", "experience"=>0, "prevWorkName"=>nil, "prevWorkSalary"=>nil}

        acc = ListEmployee.new

        self.connection.query("SELECT * FROM Employee;").each do
            |row|
            name = row["name"]
            birtdate = row["birtdate"]
            birtdate = "#{birtdate.mday}.#{birtdate.mon}.#{birtdate.year}"
            phone = row["phone"]
            email = row["email"]
            passport = row["passport"]
            speciality = row["speciality"]
            adress = row["adress"]
            experience = row["experience"]

            if experience == 0
                acc.list.push Employee.new(name, birtdate, phone, email, passport, speciality, adress)
            else
                prevWorkName = row["prevWorkName"]
                prevWorkSalary = row["prevWorkSalary"]
                acc.list.push emp = Employee.new(name, birtdate, phone, email, passport, speciality, adress, prevWorkName, prevWorkSalary)
            end
        end

        acc
    end

    def add(emp)
        # insert into `Employee` values(NULL, CURDATE(), '+7-918-2353764',
        # 'some@mail.ru', '1234 123456', 'santex', 'Krasnodar', 0, NULL, NULL);

        preWork = "NULL"
        preSal = "NULL"
        if emp.experience != 0
            preWork = "'#{emp.prevWorkName}'"
            preSal = emp.prevWorkSalary
        end

        date = Date.strptime(emp.birthdate, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"
        str = "INSERT INTO `Employee` values(NULL, '#{emp.name}', '#{date}', '#{emp.phone}', '#{emp.email}', '#{emp.passport}', '#{emp.speciality}', '#{emp.adress}', #{emp.experience}, #{preWork}, #{preSal});"
        self.connection.query(str)
    end

    def delete emp
        self.connection.query("DELETE FROM `Employee` WHERE passport = '#{emp.passport}'")
    end

    def changeNode(emp, lastPas = nil)
        date = Date.strptime(emp.birthdate, '%d.%m.%Y')
        date = "#{date.year}-#{date.mon}-#{date.mday}"

        preWork = "NULL"
        preSal = "NULL"
        if emp.experience != 0
            preWork = "'#{emp.prevWorkName}'"
            preSal = emp.prevWorkSalary
        end

        passport = if lastPas == nil then emp.passport else lastPas end

        str = "UPDATE `Employee` SET "
        str += "name = '#{emp.name}', "
        str += "birtdate = '#{date}', "
        str += "phone = '#{emp.phone}', "
        str += "email = '#{emp.email}', "
        str += "passport = '#{emp.passport}', "
        str += "speciality = '#{emp.speciality}', "
        str += "adress = '#{emp.adress}', "
        str += "experience = #{emp.experience}, "
        str += "prevWorkName = #{preWork}, "
        str += "prevWorkSalary = #{preSal} "
        str += "WHERE passport = '#{passport}';"

        self.connection.query(str)
    end
end
