require 'openssl'
require 'mysql2'

require './TerminalViewListEmployee.rb'

tvle = TerminalViewListEmployee.new "dump.yaml"
message = "Введите команду (show - показать, add - добавить, dump - выгрузка в файл, load - загрузка из файла, find - поиск, edit - редактирование по id, delete - удаление по id, sort - сортировка exit - выход): "
print message
command = STDIN.gets.strip

while !command.empty?
    if command == "show"
        tvle.show

    elsif command == "dump"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        tvle.fileDump(filename)

    elsif command == "load"
        print "Введите название файла: "
        filename = STDIN.gets.strip
        tvle.fileLoad(filename)

    elsif command == "add"
        tvle.consoleInput

    elsif command == "find"
        tvle.find

    elsif command == "edit"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            tvle.edit(id)
        rescue Exception => exp
            puts exp.message
        end

    elsif command == "delete"
        print "Введите номер пользователя (самая левая колонка в show): "
        begin
            id = STDIN.gets.strip.to_i
            tvle.delete(id)
        rescue Exception => exp
            puts exp.message
        end

    elsif command == "sort"
        tvle.sort

    elsif command == "exit"
        exit 0
    end

    print message
    command = STDIN.gets.strip
end

tvle.dumpAs "dump.yaml"
