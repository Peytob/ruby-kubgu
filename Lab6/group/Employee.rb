require 'json'
require 'yaml'
require 'nokogiri'

class Employee

    attr_accessor :name, :birthdate, :phone, :passport, :speciality,
        :adress, :experience

    # Консруктор №1: по верхнему списку до опыта.
    # Констуруктор №2: первый + последние три

    def initialize(*args)
        raise "Количество аргументов должно равняться 7 или 10" unless (args.length == 7 || args.length == 10)

        if args.length >= 7
            self.name = args[0]
            self.birthdate = args[1]
            self.phone = args[2]
            self.email = args[3]
            self.passport = args[4]
            @speciality = args[5]
            @adress = args[6]
            @experience = 0
        end

        if args.length >= 10
            return if args[8] == nil || args[8] == nil

            @experience = args[7].to_i

            class << self
                attr_accessor :prevWorkName, :prevWorkSalary
            end

            @prevWorkName = args[8]
            @prevWorkSalary = args[9].to_i
        end
    end

    def self.isPhoneNumberCorrent?(phone)
        phone =~ /^(\+\s*?7|8)([- ()]*\d){10}$/ ? true : false
    end

    def self.prettyPhoneNumber(phone)
        raise "Неверный формат номера телефона: #{phone}" unless Employee.isPhoneNumberCorrent? phone.strip

        numbersOnly = phone.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("")
        res = "#{numbersOnly[0]}-#{numbersOnly[1..3]}-#{numbersOnly[4..numbersOnly.length - 1]}"
        res = "+7" + res[1, res.length - 1] if phone[0] == '8'
        res = "+" + res if phone[0] == '+'
        res
    end

    def phone=(phone)
        @phone = Employee.prettyPhoneNumber(phone)
    end

    def phone
        @phone
    end

    def self.isEmailCorrect?(email)
        email =~ /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ ? true : false
    end

    def self.prettyEmail(email)
        raise "Неверный формат почты: #{email}" unless Employee.isEmailCorrect? email.strip

        email.downcase
    end

    def email=(email)
        @email = Employee.prettyEmail(email)
    end

    def email
        @email
    end

    def self.isNameCorrect?(name)
        name =~ /([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)\s+([a-яА-Я]+\s*-?\s*[a-яА-Я]+|[a-яА-Я]+)/ ? true : false
    end

    def self.prettyName(name)
        raise "Неверный формат имени: #{name}" unless Employee.isNameCorrect? name.strip
        name = name.downcase
        name = name.gsub(/\s\s+/, " ").gsub(/\b./, &:upcase).gsub(/(?<=[а-яА-Я])\s-\s(?=[а-яА-Я])/, "-")
    end

    def name=(name)
        @name = Employee.prettyName(name)
    end

    def name
        @name
    end

    def self.isPassportCorrect?(passport)
        passport =~ /(\s*\d\s*){10}/ ? true : false
    end

    def self.prettyPassport(passport)
        raise "Неверный формат паспорта: #{passport}" unless Employee.isPassportCorrect? passport.strip
        passport.chars.filter { |ch| ?0.ord <= ch.ord && ch.ord <= ?9.ord }.join("").insert(4, " ")
    end

    def passport=(passport)
        @passport = Employee.prettyPassport(passport)
    end

    def passport
        @passport
    end

    def self.isBirthdateCorrect?(birthdate)
        birthdate =~ /([0-2]?\d|3[0-1])\.(0?\d|1[0-2])\.(\d{4}|\d{2})/ ? true : false
    end

    def self.prettyBirthdate(birthdate)
        raise "Неверный формат даты: #{birthdate}" unless Employee.isBirthdateCorrect? birthdate.strip
        birthdate = birthdate.split('.').map {|i| i.to_i.to_s }
        birthdate[0] = "0" + birthdate[0] if birthdate[0].to_i < 10
        birthdate[1] = "0" + birthdate[1] if birthdate[1].to_i < 10
        birthdate[2] = "20" + birthdate[2] if birthdate[2].to_i < 100
        birthdate.join(".")
    end

    def birthdate=(birthdate)
        @birthdate = Employee.prettyBirthdate(birthdate)
    end

    def birthdate
        @birthdate
    end

    def to_s
        t = "ФИО: #{self.name} (#{@speciality}); ДР: #{@birthdate}; " +
            "Контакты: #{@phone}; #{@email}; #{@adress}; Стаж: #{@experience} "
        t += "Предудыщая работа: #{@prevWorkName}; ЗП: #{@prevWorkSalary}" if @experience > 0
        t
    end

    def to_json
        emp = {
            "name" => self.name,
            "birthdate" => self.birthdate,
            "phone" => self.phone,
            "email" => self.email,
            "passport" => self.passport,
            "speciality" => self.speciality,
            "adress" => self.adress
        }

        if self.experience == 0
            emp["experience"] = 0
            emp["prevWorkName"] = nil
            emp["prevWorkSalary"] = nil

        else
            emp["experience"] = self.experience
            emp["prevWorkName"] = self.prevWorkName
            emp["prevWorkSalary"] = self.prevWorkSalary
        end

        emp.to_json
    end

    def to_xmlNode doc
        root = Nokogiri::XML::Node.new "employee", doc
        (root.add_child Nokogiri::XML::Node.new "name", doc).content = self.name
        (root.add_child Nokogiri::XML::Node.new "birthdate", doc).content = self.birthdate
        (root.add_child Nokogiri::XML::Node.new "phone", doc).content = self.phone
        (root.add_child Nokogiri::XML::Node.new "email", doc).content = self.email
        (root.add_child Nokogiri::XML::Node.new "passport", doc).content = self.passport
        (root.add_child Nokogiri::XML::Node.new "speciality", doc).content = self.speciality
        (root.add_child Nokogiri::XML::Node.new "adress", doc).content = self.adress
        (root.add_child Nokogiri::XML::Node.new "experience", doc).content = self.experience
    
        if self.experience != 0
            (root.add_child Nokogiri::XML::Node.new "prevWorkName", doc).content = self.prevWorkName
            (root.add_child Nokogiri::XML::Node.new "prevWorkSalary", doc).content = self.prevWorkSalary
        end

        root
    end

    def eql?(other)
        return false if other.class != self.class
        return false if other.experience != self.experience

        # Тупой момент, конечно, но все же. Тут, наверное, можно проверять только паспорт,
        # так как он уникален в БД. Но на всякий случай (плюс класс можно не только в связке с БД
        # использовать) проверяю все.

        r = true
        r &= self.name == other.name
        r &= self.birthdate == other.birthdate
        r &= self.phone == other.phone
        r &= self.email == other.email
        r &= self.passport == other.passport
        r &= self.speciality == other.speciality
        r &= self.adress == other.adress

        if self.experience != 0 # У второго тоже не ноль - см выше
            r &= self.prevWorkName == other.prevWorkName
            r &= self.prevWorkSalary == other.prevWorkSalary
        end

        r
    end
end
