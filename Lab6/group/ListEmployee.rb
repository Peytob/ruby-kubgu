require 'date'
require 'yaml'
require 'json'
require 'nokogiri'

require './Employee.rb'

class ListEmployee
    attr_accessor :list

    def initialize
        self.list = Array.new
    end

    def consoleInput
        print "Введите имя: "
        name = STDIN.gets.strip

        print "Введите дату рождения: "
        birthdate = STDIN.gets.strip
        
        print "Введите мобильный телефон: "
        phones = STDIN.gets.strip

        print "Введите почту: "
        emails = STDIN.gets.strip
        
        print "Введите паспортные данные: "
        passports = STDIN.gets.strip

        print "Введите специальность: "
        specialities = STDIN.gets.strip
        
        puts "Введите адрес проживания: "
        adresses = STDIN.gets.strip

        begin
            self.list << Employee.new(name, birthdate, phones, emails, passports, specialities, adresses)
        rescue RuntimeError => e
            puts e.message
        rescue Exception => e
            puts "Что-то явно пошло не так, но ввод, вроде, верный: #{e.message}"
        end

        return self.list.last
    end

    def show
        if self.list.empty?
            puts "Empty"
        
        else
            nameFieldSize = list.map { |i| i.name.length }.max + 2
            birthdate = 16
            phones = 16
            emails = list.map { |i| i.email.length }.max + 2
            passports = 13
            specialities = [list.map { |i| i.speciality.length }.max + 2, 15].max
            adresses = list.map { |i| i.adress.length }.max + 2
            exp = 6
            salary = 10

            str = "#".to_s.center(5)
            str += "Имя".center(nameFieldSize)
            str += "Дата рождения".center(birthdate)
            str += "Телефон".center(phones)
            str += "Почта".center(emails)
            str += "Паспорт".center(passports)
            str += "Специальность".center(specialities)
            str += "Адрес".center(adresses)
            str += "Опыт".center(exp)
            str += "Прош. спец".center(specialities)
            str += "Зарплата".center(salary)

            puts str

            list.each_with_index do
                |elem, index|
                
                str = index.to_s.center(5)
                str += elem.name.center(nameFieldSize)
                str += elem.birthdate.center(birthdate)
                str += elem.phone.center(phones)
                str += elem.email.center(emails)
                str += elem.passport.center(passports)
                str += elem.speciality.center(specialities)
                str += elem.adress.center(adresses)
                str += elem.experience.to_s.center(exp)

                if elem.experience != 0
                    str += elem.prevWorkName.center(specialities)
                    str += elem.prevWorkSalary.to_s.center(salary)
                else
                    str += "X".center(specialities)
                    str += "X".to_s.center(salary)
                end
                puts str
            end
        end
    end

    def findByName(value)
        findLst = ListEmployee.new
        findLst.list = self.list.filter { |emp| emp.name === value }
        findLst
    end

    def findByEmail(value)
        findLst = ListEmployee.new
        findLst.list = self.list.filter { |emp| emp.email === value }
        findLst
    end

    def findByPassport(value)
        findLst = ListEmployee.new
        findLst.list = self.list.filter { |emp| emp.passport === value }
        findLst
    end

    def findByPhoneNumber(value)
        findLst = ListEmployee.new
        findLst.list = self.list.filter { |emp| emp.phone === value }
        findLst
    end

    def sortByName()
        self.list.sort_by! { |emp| emp.name }
    end

    def sortByPhone()
        self.list.sort_by! { |emp| emp.phone }
    end

    def writeListYAML(filename)
        File.open(filename, "w") do
            |file|
            YAML.dump(self.list, file)
        end
    end

    def readListYAML(filename)
        self.list = YAML.load_file(filename)
    end

    def to_json
        jsonList = self.list.map { |i| i.to_json }.join(", ")
        "[" + jsonList + "]"
        # Почему оно переводит массив в что-то типа {["{}", "{}", ...]} ???
    end

    def writeListJSON(filename)
        File.open(filename, "w") do
            |file|
            file.write(self.to_json) # Переводит emp в строку
        end
    end

    def readListJSON(filename)
        json = ""
        File.open(filename, 'r') do
            |file|

            json = file.read
        end

        JSON.parse(json).each do
            |empJson|
            
            puts empJson
            emp = Employee.new(empJson["name"], empJson["birthdate"], empJson["phone"], empJson["email"], empJson["passport"], empJson["speciality"], empJson["adress"], empJson["experience"], empJson["prevWorkName"], empJson["prevWorkSalary"])
            self.list.push emp
        end
    end

    def writeListXML(filename)
        doc = Nokogiri::XML "<employees></employees>"
        root = doc.at_css("employees")

        self.list.each { |emp|  root.add_child (emp.to_xmlNode doc) }

        File.open(filename, 'w') do
            |file|

            file.write(doc)
        end
    end

    def readListXML(filename)
        doc = File.open(filename, 'r').read
        doc = Nokogiri::XML doc

        return if !doc.errors.empty?

        # puts doc

        doc.css("employee").map do
            |node|

            puts node.css("name").text
            puts node.css("agdsg").text.dump

            name = node.css("name").text
            birthdate = node.css("birthdate").text
            phone = node.css("phone").text
            email = node.css("email").text
            passport = node.css("passport").text
            speciality = node.css("speciality").text
            adress = node.css("adress").text
            experience = node.css("experience").text
            prevWorkName = node.css("prevWorkName").text
            prevWorkSalary = node.css("prevWorkSalary").text

            self.list.push Employee.new(name, birthdate, phone, email, passport, speciality, adress, experience, prevWorkName, prevWorkSalary)
        end
    end
end
