class Crypto
    private
        attr_writer :key, :iv
        attr_accessor :cipher
    public
        attr_reader :key, :iv

    def initialize(key = nil, iv = nil)
        self.cipher = OpenSSL::Cipher.new 'AES-256-CBC'
        if key == nil then generateKey else self.key = key end
        self.iv = iv if iv != nil
    end

    def generateKey
        self.iv = self.cipher.random_iv
        pwd = 'some hopefully not to easily guessable password'
        salt = OpenSSL::Random.random_bytes 16
        iter = 20000
        key_len = self.cipher.key_len
        digest = OpenSSL::Digest::SHA256.new
        self.key = OpenSSL::PKCS5.pbkdf2_hmac(pwd, salt, iter, key_len, digest)
    end

    def encrypt text
        self.cipher.encrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        encrypted = self.cipher.update text
        encrypted << self.cipher.final
        encrypted
    end

    def decrypt encrypted
        self.cipher.decrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        decrypted = self.cipher.update encrypted
        decrypted << self.cipher.final
        decrypted
    end

    def exportKeyToFile(filenameKey, filenameIv)
        File.open(filenameKey, 'wb') do
            |file|
            file.write(self.key)
        end

        File.open(filenameIv, 'wb') do
            |file|
            file.write(self.iv)
        end
    end

    def importKeyFromFile(filenameKey, filenameIv)
        self.key = File.open(filenameKey, 'rb').read
        self.iv = File.open(filenameIv, 'rb').read
    end
end
