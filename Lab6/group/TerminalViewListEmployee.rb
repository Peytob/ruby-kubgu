require './Crypto.rb'
require './ListEmployee.rb'
require './DatabaseConnection.rb'

class TerminalViewListEmployee
private
    attr_accessor :listEmp, :database

public

    def initialize dumpFile
        begin
            self.database = DatabaseConnection.new("user12", "34klq*")
            self.listEmp = self.database.readListDB()
        rescue Exception => exp
            puts "Ошибка при инициализации: #{exp}"
            print "Загрузка из файла дампа: #{dumpFile} (Д/н)?"
            t = STDIN.gets.strip.downcase
            abort "На нет и суда нет" if t == "" || t == "н" || t == "n"
            self.listEmp = ListEmployee.new
            self.loadFrom(dumpFile)
            return
        end

        if File.exists? dumpFile
            fileEmp = ListEmployee.new
            fileEmp.list = self.listEmp.list.map { |emp| emp.clone() }
            fileEmp.sortByName
            self.loadFrom(dumpFile)
            self.listEmp.sortByName
            equal = self.listEmp.list.zip(fileEmp.list).inject(true) { |acc, first| acc & first[0].eql?(first[1]) }
            equal &= self.listEmp.list.size == fileEmp.list.size
            if !equal
                print "Данные в дампе и БД не являются одинаковыми! Откуда брать (Ф / бд)? "
                source = STDIN.gets.strip.downcase
                if source == "бд"
                    self.listEmp = fileEmp
                else
                    self.database.clear
                    self.listEmp.each { |emp| self.database.db}
                end
            end
        end
    end 

    def fileDump(filename)
        # Разбиваются по строкам, так как нет символа-сепаратора, недоступного во всех
        # полях поэтому бьем по новым строкам

        crp = Crypto.new
        crp.exportKeyToFile("key", "iv")

        File.open(filename, 'w') do
            |file|

            self.listEmp.list.each do
                |emp|

                file.write(emp.name + "\n")
                file.write(emp.birthdate + "\n")
                file.write(emp.phone + "\n")
                file.write(emp.email + "\n")
                file.write(crp.encrypt(emp.passport) + "\n")
                file.write(emp.speciality + "\n")
                file.write(emp.adress + "\n")
                file.write(emp.experience.to_s + "\n")

                file.write(emp.prevWorkName + "\n") if emp.experience.to_i != 0
                file.write(emp.prevWorkSalary.to_s + "\n") if emp.experience.to_i != 0
            end
        end
    end

    def fileLoad(filename)
        self.listEmp.list.clear
        lines = File.open(filename, 'r').readlines.map { |line| line.chomp }
    
        crp = Crypto.new
        crp.importKeyFromFile("key", "iv")

        while !lines.empty?
            name = lines.delete_at 0
            birthdate = lines.delete_at 0
            phone = lines.delete_at 0
            email = lines.delete_at 0
            passport = crp.decrypt(lines.delete_at(0))
            speciality = lines.delete_at 0
            adress = lines.delete_at 0
            experience = (lines.delete_at 0).to_i

            if experience != 0
                prevWorkName = lines.delete_at 0
                prevWorkSalary = (lines.delete_at 0).to_i
                self.listEmp.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress, experience, prevWorkName, prevWorkSalary)
            else
                self.listEmp.list << Employee.new(name, birthdate, phone,
                    email, passport, speciality, adress)
            end
        end
    end

    def dumpAs(filename)

        type = filename.split('.')[1]

        case type
        when "yaml"
            self.listEmp.writeListYAML(filename)

        when "json"
            self.listEmp.writeListJSON(filename)

        when "xml"
            self.listEmp.writeListXML(filename)

        else
            puts "Неверный формат"
        end
    end

    def loadFrom(filename)
        self.listEmp.list.clear

        type = filename.split('.')[1]

        case type
        when "yaml"
            self.listEmp.readListYAML(filename)

        when "json"
            self.listEmp.readListJSON(filename)

        when "xml"
            self.listEmp.readListXML(filename)

        else
            puts "Неверный формат"
        end
    end

    def consoleInput
        emp = self.listEmp.consoleInput
        self.database.add(emp)
    end

    def show
        self.listEmp.show
    end

    def find
        puts <<~"END"
        Поиск по:
        1. ФИО
        2. Email
        3. Красный советский документ
        4. Номер телефона
        END

        print "Выбор: "
        
        case STDIN.gets.chomp.to_i
            when 1
                print "Имя: "
                begin
                    name = STDIN.gets.chomp
                    name = Employee.prettyName(name)
                    res = listEmp.findByName(name)
                    res.show
                rescue RuntimeError => e
                    puts e.message
                end

            when 2
                print "Адрес: "
                begin
                    email = STDIN.gets.chomp
                    email = Employee.prettyEmail(email)
                    res = listEmp.findByEmail(email)
                    res.show
                rescue RuntimeError => e
                    puts e.message
                end

        when 3
            print "Серия и номер: "
            begin
                passport = STDIN.gets.chomp
                passport = Employee.prettyPassport(passport)
                res = listEmp.findByPassport(passport)
                res.show
            rescue RuntimeError => e
                puts e.message
            end

        when 4
            print "Номер: "
            begin
                number = STDIN.gets.chomp
                number = Employee.prettyPhoneNumber(number)
                res = listEmp.findByPhoneNumber(number)
                res.show
            rescue RuntimeError => e
                puts e.message
            end
        end
    end

    def delete(id)
        emp = self.listEmp.list.delete_at id
        self.database.delete emp
    end

    def edit(id)
        puts "Редактирование пользователя с ID = #{id}"
        message = "1 - имя, 2 - дата рождения, 3 - телефон, 4 - почта, 5 - пасспорт, 6 - специальность, 7 - адрес"
        puts message
        print "Выбор: "
        ch = STDIN.gets.chomp
        pas = self.listEmp.list[id].passport

        while !ch.empty?
            el = ch.to_i

            begin
                case el
                    when 1
                        self.listEmp.list[id].name = STDIN.gets.strip()

                    when 2
                        self.listEmp.list[id].birthdate = STDIN.gets.strip()

                    when 3
                        self.listEmp.list[id].phone = STDIN.gets.strip()

                    when 4
                        self.listEmp.list[id].email = STDIN.gets.strip()

                    when 5
                        self.listEmp.list[id].passport = STDIN.gets.strip()

                    when 6
                        self.listEmp.list[id].specialities = STDIN.gets.strip()

                    when 7
                        self.listEmp.list[id].adress = STDIN.gets.strip()
                end

            rescue Exception => e
                puts "Данные введены неверно, проверьте ввод. #{e}"
            end

            print "Выбор: "
            ch = STDIN.gets.chomp
        end

        self.database.changeNode(self.listEmp.list[id], pas)
    end

    def sort
        message = "1 - имя, 2 - телефон"
        puts message
        print "Выбор: "
        ch = STDIN.gets.chomp

        while !ch.empty?
            el = ch.to_i

            case el
                when 1
                    self.listEmp.sortByName
                when 2
                    self.listEmp.sortByPhone
            end

            print "Выбор: "
            ch = STDIN.gets.chomp
        end
    end
end
