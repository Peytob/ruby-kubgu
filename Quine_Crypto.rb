require "openssl"

class Crypto
    private
        attr_accessor :cipher
    public
        attr_accessor :key, :iv, :password

    def initialize(password)
        self.cipher = OpenSSL::Cipher.new 'AES-256-CBC'
        self.password = password
        self.generateKey()
    end

    def generateKey
        self.iv = '0123456789012345'
        salt = '0123456789012345'
        iter = 20000
        key_len = self.cipher.key_len
        digest = OpenSSL::Digest::SHA256.new
        self.key = OpenSSL::PKCS5.pbkdf2_hmac(self.password, salt, iter, key_len, digest)
    end

    def encrypt text
        self.cipher.encrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        encrypted = self.cipher.update text
        encrypted << self.cipher.final
        encrypted
    end

    def decrypt encrypted
        self.cipher.decrypt
        self.cipher.key = self.key
        self.cipher.iv = self.iv
        decrypted = self.cipher.update encrypted
        decrypted << self.cipher.final
        decrypted
    end
end

puts IO.read($0)
