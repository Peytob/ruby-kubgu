class String
    def shuffle()
        return self if self.length < 3

        chars = self.chars

        holdFirst = chars.shift
        holdLast = chars.pop

        ([holdFirst] + chars.shuffle + [holdLast]).join("")
    end
end

# 6
# Дана строка в которой записаны слова через пробел. Необходимо
# перемешать в каждом слове все символы в случайном порядке кроме
# первого и последнего.
def case_6(str)
    str.split.map { |w| w.shuffle }
end


# 12
# Дана строка в которой содержатся цифры и буквы. Необходимо
# расположить все цифры в начале строки, а буквы -- в конце.
# Замечу, об отсутсвии сортировки после этого никто ничего не говорил
def case_12(str)
    str.split('').sort.join("")
end

print "Введите номер задачи: "
quest = STDIN.gets.to_i

print "Введите строку: "
str = STDIN.gets.chomp.strip

choose = {
    6 => lambda { |var| puts "6: #{case_6(var)}",
    12 => lambda { |var| puts "12: #{case_12(var)}" },
}
choose.default = lambda { |var| puts "Хм. Я не знаю такой опции..." }
choose[quest][str]
