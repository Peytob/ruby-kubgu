def readAArray(n)
    puts "Вводите значения, разделяя их Enter. Отсутствие ввода обозначает его конец."
    acc = Array.new

    for i in 1 .. n
        print "Введите элемент ##{i}: "
        acc << STDIN.gets.chomp.strip.downcase.to_i
    end

    acc
end

def getMinimalElement(array)
    array.min
end

def getMaximalElements(array)
    array.max
end

def getElemetsSum(array)
    array.inject(0) {|acc, i| acc += i }
end

def getElemetsMul(array)
    array.inject(1) {|acc, i| acc *= i }
end

abort "Введите количество элементов массива!" if ARGV.length < 1

array = readAArray(ARGV[0].to_i)

puts <<~"END"
getMinimalElement:  #{getMinimalElement(array)}
getMaximalElements: #{getMaximalElements(array)}
getElemetsSum:      #{getElemetsSum(array)}
getElemetsMul:      #{getElemetsMul(array)}

END

# Способы добавление элемента в массив
array = Array.new
puts "Изначально: #{array}"
array << 1
puts "array << 1: #{array}"
array.push 2
puts "array.push 2: #{array}"
array[2] = 3
puts "array[2] = 3: #{array} (именно присвоение следующему индексу)"
array.unshift 4
puts "array.unshift 4: #{array}"
array += [5]
puts "array += [5]: #{array}"
puts "array.insert 6: #{array}"
array.insert 6
