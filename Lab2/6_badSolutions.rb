def getMax(array)
    max = array[0]
    maxIndex = 0
    for i in 1 .. array.length - 1
        max, maxIndex = array[i], i if max > array[i]
    end
    return max, maxIndex
end

def getMin(array)
    min = array[0]
    minIndex = 0
    for i in 1 .. array.length - 1
        min, minIndex = array[i], i if min < array[i]
    end
    return min, minIndex
end

def countElement(array, element)
    count = 0
    for i in array
        count += 1 if i == element
    end
    return count
end

# ==========================

# 1.12
# Дан целочисленный массив. Необходимо переставить в обратном
# порядке элементы массива, расположенные между его минимальным и
# максимальным элементами.
def case_1_12(array)
    from = getMin(array)[1]
    to = getMax(array)[1]
    from, to = if to > from then [from, to] else [to, from] end

    elementsInside = to - from - 1
    for i in 1 .. elementsInside / 2
        array[from + i], array[from + (elementsInside - i + 1)] = array[from + (elementsInside - i + 1)], array[from + i]
    end
    return array
end

# 1.24
# Дан целочисленный массив. Необходимо найти два наибольших
# элемента.
def case_1_24(array)
    max = [0, 0]

    for i in 1 .. array.length - 1
        if array[max[0]] < array[i]
            if array[i] > array[max[1]]
                max[0] = max[1];
                max[1] = i;
            else
                max[0] = i;
            end
        end
    end

    return [array[max[0]], array[max[1]]]
end

# 1.36
# Дан целочисленный массив. Необходимо найти максимальный
# нечетный элемент.
# Считаем, что отчет начинается с 0. В любом случае разница в odd / even
def case_1_36(array)
    max = array[0]
    for i in 1 .. array.length - 1
        max = array[i] if max < array[i] && array[i].odd?
    end
    return max
end

# 1.48
# Для введенного списка построить список с номерами элемента, который
# повторяется наибольшее число раз.
def case_1_48(array)
    element = array[0]
    count = 0

    for i in 0 .. array.length - 1
        localCount = countElement(array, array[i])
        element, count = array[i], localCount if localCount > count
    end
    
    acc = Array.new
    for i in 0 .. array.length - 1
        acc << i if array[i] == element
    end

    return acc
end

# 1.60
# Дан список. Построить массив из элементов, делящихся на свой
# номер и встречающихся в исходном массиве 1 раз.
def case_1_60(array)
    acc = Array.new

    for i in 1 .. array.length - 1 # С 1 тк на 0 делить нельзя
        acc << array[i] if array[i] % i == 0 && countElement(array, i) == 1
    end

    return acc
end

# =========================

def readAArrayFile(filename, encoding="UTF-8")
    return Array.New if !File.exist?(filename)
    file = File.new(filename, "r:#{encoding}")
    file.readlines.map{ |i| i.strip }.filter{ |i| !i.empty? }.map{ |i| i.to_i }
end

def readAArrayConsole()
    puts "Вводите значения, разделяя их Enter. Отсутствие ввода обозначает его конец."
    input = STDIN.gets.chomp.strip.downcase
    acc = Array.new

    while input != ""
        acc << input.to_i
        input = STDIN.gets.chomp.strip.downcase
    end

    acc
end

puts case_1_12([-12, 54, 12, 46, 100, 1, 2, 3, 4, 5, -54, 87]).to_s
puts case_1_24([-12, 54, 12, 46, 100, 1, 2, 3, 4, 5, -54, 87]).to_s
puts case_1_36([-12, 54, 12, 46, 100, 1, 2, 3, 4, 5, -54, 87]).to_s
puts case_1_48([12, 42, 12, 54, 54, 34, 12, 42, 12, 34]).to_s
puts case_1_60([1, 1, 2 * 12, 3 * 5 + 1, 4 * 2 + 55, 5 * 6, 6 * 5, 7 * 7, 8, 8, 10 * 10]).to_s

if ARGV.length < 2
    abort "Введите способ чтения и номер задачи! console - из консоли; file #filename - из файла"
end

case ARGV[0].chomp.strip.downcase
    when "console"
        array = readAArrayConsole()
        quest = ARGV[1].to_i

    when "file"
        abort "Введите название файла" if ARGV.length < 2
        array = readAArrayFile(ARGV[1])
        abort "Введите номер задачи!" if ARGV.length < 3
        quest = ARGV[2].to_i

    else
        abort "Неизвестный способ чтения..."

    end

choose = {
    12 => lambda { |var| puts "1.12: #{case_1_12(var)}" },
    24 => lambda { |var| puts "1.24: #{case_1_24(var)}" },
    36 => lambda { |var| puts "1.36: #{case_1_36(var)}" },
    48 => lambda { |var| puts "1.48: #{case_1_48(var)}" },
    60 => lambda { |var| puts "1.60: #{case_1_60(var)}" },
}
choose.default = lambda { |var| puts "Хм. Я не знаю такой опции..." }
choose[quest][array]
