def readAArray()
    puts "Вводите значения, разделяя их Enter. Отсутствие ввода обозначает его конец."
    input = STDIN.gets.chomp.strip.downcase
    acc = Array.new

    while input != ""
        acc << input.to_i
        input = STDIN.gets.chomp.strip.downcase
    end

    acc
end

def getMinimalElement(array)
    acc = Float::INFINITY
    for i in array
        acc = i < acc ? i : acc
    end
    acc
end

def getMaximalElements(array)
    acc = -Float::INFINITY
    for i in array
        acc = i > acc ? i : acc
    end
    acc
end

def getElemetsSum(array)
    acc = 0
    for i in array
        acc += i
    end
    acc
end

def getElemetsMul(array)
    acc = 1
    for i in array
        acc *= i
    end
    acc
end

array = readAArray()

puts <<~"END"
getMinimalElement:  #{getMinimalElement(array)}
getMaximalElements: #{getMaximalElements(array)}
getElemetsSum:      #{getElemetsSum(array)}
getElemetsMul:      #{getElemetsMul(array)}
END
