# 6
# Дана строка. Необходимо подсчитать количество чисел в этой
# строке, значение которых больше 5
def case_6(str)
    str.split().filter { |w| w =~ /^-?\d+$/ }.count{ |w| w.to_i > 5 }
end

# 12
# Дана строка. Необходимо найти те символы кириллицы, которые не
# задействованы в данной строке.
def case_12(str)
    russianLetters = ('а' .. 'я').to_a
    onlyRussianPart = str.downcase.strip.chars.filter { |ch| ?а.ord <= ch.ord && ch.ord <= ?я.ord }
    russianLetters - onlyRussianPart
end

print "Введите строку: "
str = STDIN.gets.chomp.strip

puts case_6(str)
puts case_12(str)

# puts case_6("sdfsdg 5 25 sdg -532 -3 etgg sdk 325 -52 yhth34 3-543 3453 2-52 dsg 4")
# puts case_12("sdlfjk sdlkf sdgk sdg 9ваир2045  t4hoi 34фукt 34g3сича ge  gsнглпgj sывпlkdgj 43t").to_s