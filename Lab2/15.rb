def readAArray(filename, encoding="UTF-8")
    return Array.New if !File.exist?(filename)
    file = File.new(filename, "r:#{encoding}")
    file.readlines.map { |line| line.strip }.filter { |str| !str.empty? }
end

# Строки проходят strip, исключаются пустые. Слово считаем за последовательность букв

# Частота встречаемости букв

$alphabet = {
    ?A => 8.1 / 100.0, # Перевод из %
    ?K => 0.4 / 100.0, # Перевод из %
    ?V => 0.9 / 100.0, # Перевод из %
    ?B => 1.4 / 100.0, # Перевод из %
    ?L => 3.4 / 100.0, # Перевод из %
    ?W => 1.5 / 100.0, # Перевод из %
    ?C => 2.7 / 100.0, # Перевод из %
    ?M => 2.5 / 100.0, # Перевод из %
    ?X => 0.2 / 100.0, # Перевод из %
    ?D => 3.9 / 100.0, # Перевод из %
    ?N => 7.2 / 100.0, # Перевод из %
    ?Y => 1.9 / 100.0, # Перевод из %
    ?E => 13.0 / 100.0, # Перевод из %
    ?O => 7.9 / 100.0, # Перевод из %
    ?Z => 0.1 / 100.0, # Перевод из %
    ?F => 2.9 / 100.0, # Перевод из %
    ?P => 2.0 / 100.0, # Перевод из %
    ?G => 2.0 / 100.0, # Перевод из %
    ?R => 6.9 / 100.0, # Перевод из %
    ?H => 5.2 / 100.0, # Перевод из %
    ?S => 6.1 / 100.0, # Перевод из %
    ?I => 6.5 / 100.0, # Перевод из %
    ?T => 10.5 / 100.0, # Перевод из %
    ?J => 0.2 / 100.0, # Перевод из %
    ?U => 2.4 / 100.0, # Перевод из %
}

def isLetter(l)
    l.upcase.ord >= ?A.ord && l.upcase.ord <= ?Z.ord
end

# 3
# В порядке увеличения разницы между частотой наиболее часто
# встречаемого символа в строке и частотой его появления в алфавите
def case_3(text)
    text = text.sort_by do
        |line|
        result = -1 # Костыль, тк я не смог нормально сделать return тут)0

        letter, frequency = line.chars.uniq.filter { |l| isLetter(l) }.map { |letter| [letter.upcase, line.count(letter)] }.first
        unless letter.nil? || frequency.nil? # Пропуск строк вообще без букв
            frequency = frequency.to_f / line.chars.filter { |l| isLetter(l) }.count
            result = (frequency - $alphabet[letter]).abs
        end

        result
    end

    puts text
end

# 6
# В порядке увеличения медианного значения выборки строк (про-
# шлое медианное значение удаляется из выборки и производится поиск нового
# медианного значения)
def case_6(text)
    # А тут есть момент. Размеры массива text меняются и имеется два выбора:
    # учитывать уже отсортированные строки в ином массиве или же просто
    # удалять их из копии исходного, но использовать цикл while. Первый случай
    # жрет заметно больше из-за include? на каждой итерации для кучи строк, кроме
    # того он заметно сложнее в логике.

    textClone = text.clone
    text.clear
    while !textClone.empty?
       textClone.sort_by do
            |line|

            line.chars.map {|i| i.ord }.reduce(0, :+)
       end
       medianIndex = textClone.length / 2
       text << textClone.delete_at(medianIndex)
    end
end

# 9
# В порядке увеличения квадратичного отклонения между наиболь-
# шим ASCII-кодом символа строки и разницы в ASCII-кодах пар зеркально рас-
# положенных символов строки (относительно ее середины)
def case_9(text)
    text = text.sort_by do
        |line|

        maximalAscii = line.chars.max.ord
        line.chars.zip(line.reverse.chars).inject(0) { |acc, i| acc += ((i[0].ord - i[1].ord).abs - maximalAscii) ** 2 } / line.length.to_f
    end

    puts text
end

# 10
# В порядке увеличения среднего количества «зеркальных» троек
# (например, «ada») символов в строке
def case_10(text)
    text.sort_by do
        |line|

        acc = 0
        for i in 1 .. line.length - 1 - 1
            acc += 1 if line[i - 1] == line[i + 1] && isLetter(line[i - 1]) && isLetter(line[i])
        end
        acc
    end

    puts text
end

lines = readAArray("wordsFile.txt")
# case_3(lines)
case_6(lines)
# case_9(lines)
# case_10(lines)
