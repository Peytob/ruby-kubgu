def readAArray(filename, encoding="UTF-8")
    return Array.New if !File.exist?(filename)
    file = File.new(filename, "r:#{encoding}")
    file.readlines.map { |line| line.strip }.filter { |str| !str.empty? }
end

# Строки проходят strip, исключаются пустые

lines = readAArray("wordsFile.txt").sort_by { |line| line.length }
puts lines
