def readAArray()
    puts "Вводите значения, разделяя их Enter. Отсутствие ввода обозначает его конец."
    input = STDIN.gets.chomp.strip.downcase
    acc = Array.new

    while input != ""
        acc << input.to_i
        input = STDIN.gets.chomp.strip.downcase
    end

    acc

    # Спросить про %W(#{gets})
end

def getMinimalElement(array)
    array.min
end

def getMaximalElements(array)
    array.max
end

def getElemetsSum(array)
    array.inject(0) { |acc, i| acc += i }
end

def getElemetsMul(array)
    array.inject(1) { |acc, i| acc *= i }
end

array = readAArray()

puts <<~"END"
getMinimalElement:  #{getMinimalElement(array)}
getMaximalElements: #{getMaximalElements(array)}
getElemetsSum:      #{getElemetsSum(array)}
getElemetsMul:      #{getElemetsMul(array)}
END
