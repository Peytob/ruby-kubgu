def readAArray(filename, encoding="UTF-8")
    return Array.New if !File.exist?(filename)
    file = File.new(filename, "r:#{encoding}")
    file.readlines.map{ |i| i.strip }.filter{ |i| !i.empty? }.map{ |i| i.to_i }
end

def getMinimalElement(array)
    array.min
end

def getMaximalElements(array)
    array.max
end

def getElemetsSum(array)
    array.inject(0) {|acc, i| acc += i }
end

def getElemetsMul(array)
    array.inject(1) {|acc, i| acc *= i }
end

if ARGV.length < 1
    abort "Укажите имя файла!"
end

array = readAArray(ARGV[0])

puts <<~"END"
getMinimalElement:  #{getMinimalElement(array)}
getMaximalElements: #{getMaximalElements(array)}
getElemetsSum:      #{getElemetsSum(array)}
getElemetsMul:      #{getElemetsMul(array)}
END
