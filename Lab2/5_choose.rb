def readAArrayFile(filename, encoding="UTF-8")
    return Array.New if !File.exist?(filename)
    file = File.new(filename, "r:#{encoding}")
    file.readlines.map{ |i| i.strip }.filter{ |i| !i.empty? }.map{ |i| i.to_i }
end

def readAArrayConsole()
    puts "Вводите значения, разделяя их Enter. Отсутствие ввода обозначает его конец."
    input = STDIN.gets.chomp.strip.downcase
    acc = Array.new

    while input != ""
        acc << input.to_i
        input = STDIN.gets.chomp.strip.downcase
    end

    acc
end

def readAArrayConsoleN(n)
    puts "Вводите значения, разделяя их Enter от 1 до заданного вами. Отсутствие ввода обозначает его конец."
    acc = Array.new

    for i in 1 .. n
        print "Введите элемент ##{i}: "
        acc << STDIN.gets.chomp.strip.downcase.to_i
    end

    acc
end

def getMinimalElement(array)
    array.min
end

def getMaximalElements(array)
    array.max
end

def getElemetsSum(array)
    array.inject(0) {|acc, i| acc += i }
end

def getElemetsMul(array)
    array.inject(1) {|acc, i| acc *= i }
end

if ARGV.length < 1
    abort "Введите способ чтения! console - из консоли; file #filename - из файла; console #elementsNum - из консоли в количестве elementsNum элементов"
end

# array = Null

case ARGV[0].chomp.strip.downcase
    when "console"
        array = if ARGV.length < 2 then readAArrayConsole() else readAArrayConsoleN(ARGV[1].to_i) end

    when "file"
        abort "Введите название файла" if ARGV.length < 2
        array = readAArrayFile(ARGV[1])

    else
        abort "Неизвестный способ чтения..."

    end

puts <<~"END"
getMinimalElement:  #{getMinimalElement(array)}
getMaximalElements: #{getMaximalElements(array)}
getElemetsSum:      #{getElemetsSum(array)}
getElemetsMul:      #{getElemetsMul(array)}
END
